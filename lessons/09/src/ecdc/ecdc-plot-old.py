import matplotlib.pyplot as plt
plt.style.use('classic')

from scipy import stats
import numpy as np
import pandas as pd
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()

import seaborn as sns; sns.set()

def ecdc_read(file):
    ecdc = pd.read_csv(file)
    ecdc['dateRep'] =  pd.to_datetime(ecdc['dateRep'],dayfirst=True)
    #, format='%d%b%Y:%H:%M:%S.%f')
    # get rid of useless columns
    #ecdc.drop(columns=['day','month','year'])
    ecdc = ecdc.drop(['day','month','year'], axis=1)
    # filter Europe data
    ecdc = ecdc.loc[ecdc['continentExp'] == 'Europe']
    # filter countries
    ecdc = ecdc.loc[ecdc['countriesAndTerritories'].isin(
        ['Italy','Germany','United_Kingdom','Spain','Austria']
        )]
    # countriesAndTerritories contains country name
    # normalize cases to 100k
    ecdc['cases_100k'] = 100000*ecdc['cases']/ecdc['popData2018']
    ecdc['deaths_100k'] = 100000*ecdc['deaths']/ecdc['popData2018']
    # rename columns
    ecdc = ecdc.rename(columns={'dateRep': 'date','countriesAndTerritories':'Country'})
    return ecdc

url='https://opendata.ecdc.europa.eu/covid19/casedistribution/csv/'
local='data/ecdc-daily.csv'
data = ecdc_read(local)
print(data.head())
print(data.dtypes)

# PLOTTING
fig, ax = plt.subplots(figsize = (12,6))
lp=sns.lineplot(x='date', y='cases_100k', hue='Country',
#             data=pd.melt(data, ['Year'])
             data=data, ax=ax
             )
lp.legend(loc='upper left') #, bbox_to_anchor=(1.4, 1))
ax.set_xlim(['2020-02-20', None])
ax.set_ylim([0, None])
ax.set(xlabel="date", ylabel = "cases/100k pop.")
ax.set_title('ECDC COVID-19 cases')
#ax.set_xticklabels(labels=x_dates, rotation=45, ha='right')
#x_dates = data['dateRep'].dt.strftime('%Y-%m-%d').sort_values().unique()
fig.autofmt_xdate()
#ax.set_xticklabels( labels=x_dates,rotation=45, ha='right')

#plt.setp(ax.get_xticklabels(), rotation=45)
#plt.savefig("figures/ecdc-europe-cases.pdf")
#lp.get_figure().savefig("figures/ecdc-europe-cases.pdf")
lp.savefig("figures/ecdc-europe-cases.pdf")
#lp.get_figure().clf() # this clears the figure

fig, ax = plt.subplots(figsize = (12,6))
lp=sns.lineplot(x='date', y='deaths', hue='Country',
#             data=pd.melt(data, ['Year'])
             data=data, ax=ax
             )
lp.legend(loc='upper left') #, bbox_to_anchor=(1.4, 1))
ax.set_xlim(['2020-02-20', None])
ax.set_ylim([0, None])
ax.set(xlabel="date", ylabel = "deaths")
ax.set_title('ECDC COVID-19 deaths')
#ax.set_xticklabels(labels=x_dates, rotation=45, ha='right')
#x_dates = data['dateRep'].dt.strftime('%Y-%m-%d').sort_values().unique()
fig.autofmt_xdate()
plt.savefig("figures/ecdc-europe-deaths.pdf")

data.to_csv('data/ecdc-filter.csv', index=False)
