#import matplotlib.pyplot as plt
#plt.style.use('classic')

from scipy import stats
import numpy as np
import pandas as pd
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()

import seaborn as sns; sns.set()

def ecdc_read(file):
    ecdc = pd.read_csv(file)
    ecdc['dateRep'] =  pd.to_datetime(ecdc['dateRep'],dayfirst=True)
    #, format='%d%b%Y:%H:%M:%S.%f')
    # get rid of useless columns
    #ecdc.drop(columns=['day','month','year'])
    ecdc = ecdc.drop(['day','month','year'], axis=1)
    # filter Europe data
    ecdc = ecdc.loc[ecdc['continentExp'] == 'Europe']
    # filter countries
    ecdc = ecdc.loc[ecdc['countriesAndTerritories'].isin(
        ['Italy','Germany','United_Kingdom','Spain','Austria']
        )]
    # countriesAndTerritories contains country name
    # normalize cases to 100k
    ecdc['cases_100k'] = 100000*ecdc['cases']/ecdc['popData2018']
    ecdc['deaths_100k'] = 100000*ecdc['deaths']/ecdc['popData2018']
    # rename columns
    ecdc = ecdc.rename(columns={'dateRep': 'date','countriesAndTerritories':'Country'})
    return ecdc

url='https://opendata.ecdc.europa.eu/covid19/casedistribution/csv/'
local='data/ecdc-daily.csv'
data = ecdc_read(local)
print(data.head())
print(data.dtypes)

# PLOTTING
#sns.set_context("talk")
#sns.set_context("notebook", font_scale=1.5, rc={"lines.linewidth": 1.5})
#sns.set_context("talk",font_scale=0.75)
lp=sns.lineplot(x='date', y='cases_100k', hue='Country',
             data=data)

lp.legend(loc='upper left')
lp.get_figure().set_figheight(6)
lp.get_figure().set_figwidth(12)

lp.axes.set_xlim(['2020-02-20', None])
lp.axes.set_ylim([0, None])
lp.axes.set_title('ECDC COVID-19 cases')
lp.axes.set(xlabel="date", ylabel = "cases/100k pop.")

lp.get_figure().autofmt_xdate()

lp.get_figure().savefig("figures/ecdc-europe-cases.pdf")

lp.get_figure().clf() # this clears the figure

lp = sns.lineplot(x='date', y='deaths', hue='Country',
             data=data
             )
lp.axes.set_xlim(['2020-02-20', None])
lp.axes.set_ylim([0, None])
lp.axes.set_title('ECDC COVID-19 deaths')
lp.axes.set(xlabel="date", ylabel = "deaths/100k pop.")

lp.get_figure().autofmt_xdate()

lp.get_figure().savefig("figures/ecdc-europe-deaths.pdf")

data.to_csv('data/ecdc-filter.csv', index=False)
