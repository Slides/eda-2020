import matplotlib.pyplot as plt
plt.style.use('classic')

from scipy import stats
import numpy as np

import seaborn as sns; sns.set()

np.random.seed(2893753681)

x = np.random.random(10)
y = 1.6*x + np.random.random(10)

R = stats.linregress(x, y)

print(R)

jp = sns.jointplot(
    x=x,
    y=y,
    kind='reg', color='r')
jp.annotate(stats.pearsonr)
# control x and y limits
plt.ylim(-0.2, 2.5)
plt.xlim(-0.2, 1.2)

plt.savefig("figures/linregress.pdf")
