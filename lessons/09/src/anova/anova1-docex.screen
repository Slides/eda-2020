Create sample data matrix y with columns that are constants, plus random 
normal disturbances with mean 0 and standard deviation 1.

>> y = meshgrid(1:5);
>> rng default;
>> y = y + normrnd(0,1,5,5)

y =

    1.5377    0.6923    1.6501    3.7950    5.6715
    2.8339    1.5664    6.0349    3.8759    3.7925
   -1.2588    2.3426    3.7254    5.4897    5.7172
    1.8622    5.5784    2.9369    5.4090    6.6302
    1.3188    4.7694    3.7147    5.4172    5.4889

>> p = anova1(y)

p =

    0.0023

>> 
>> 
>> 

The ANOVA table shows the between-groups variation (Columns) 
and within-groups variation (Error). 
SS is the sum of squares, and df is the degrees of freedom. 
The total degrees of freedom is total number of observations minus one, 
which is 25 - 1 = 24. 
The between-groups degrees of freedom is number of groups minus one, 
which is 5 - 1 = 4. 
The within-groups degrees of freedom is total degrees of freedom minus the 
between groups degrees of freedom, which is 24 - 4 = 20.

MS is the mean squared error, which is SS/df for each source of variation. 
The F-statistic is the ratio of the mean squared errors (13.4309/2.2204). 
The p-value is the probability that the test statistic can take a value 
greater than the value of the computed test statistic, i.e., P(F > 6.05). 

The small p-value of 0.0023 indicates that differences between column means 
are significant.
