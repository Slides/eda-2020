import matplotlib.pyplot as plt
plt.style.use('classic')

from scipy import stats
import numpy as np
import pandas as pd

import seaborn as sns; sns.set()

num_rows = 20
years = np.arange(1990, 1990 + num_rows))

data = pd.DataFrame({
    'Year': years,
    'A': np.random.randn(num_rows).cumsum(),
    'B': np.random.randn(num_rows).cumsum(),
    'C': np.random.randn(num_rows).cumsum(),
    'D': np.random.randn(num_rows).cumsum()})

print(data)

print(pd.melt(data, ['Year']))

# A single plot with four lines, one per measurement type, is obtained with

sns.lineplot(x='Year', y='value', hue='variable',
             data=pd.melt(data, ['Year']))

# (Note that 'value' and 'variable' are the default column names returned
# by melt, and can be adapted to your liking.)
#df2 = pd.melt(df, 'Day', var_name='Measure',
#              value_name='Value')
#
# The melt output is
#    Year variable     value
#0   1990        A -1.060029
#1   1991        A -0.108761
# ....
#78  2008        D -1.602229
#79  2009        D -4.195053

sns.get_figure().savefig("figures/multiplot.pdf")
