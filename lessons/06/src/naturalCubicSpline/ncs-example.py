import matplotlib.pyplot as plt
import seaborn as sns; sns.set()
sns.set(font="DejaVu Sans") # or you don't get minus sign in pdf - weird

import numpy as np
#import pandas as pd

N = 100
x = np.linspace(0,1,N)

# create nonlinear function (not too rapidly varying)
y_th = (((np.exp(1.2*x) + 1.5*np.sin(7*x))-1)/3)

# make some noise normally distributed
noise = np.random.normal(0, 0.15, size=N)

y = y_th + noise

from scipy.interpolate import LSQUnivariateSpline
# 4 knots
nk = 4 #+2
t = np.linspace(0,1,nk+2)[1:-1]
spl = LSQUnivariateSpline(x, y, t)

SE = np.sqrt(np.square(y_th-spl(x)).sum()/N)

# init figure (with size optimized for slides)
fig, ax = plt.subplots(figsize = (8,5))

# plot results
plt.plot(x, y, 'o',
    markerfacecolor='blue', markersize=6, color='skyblue',
    label='experimental') #, linewidth=4)
plt.plot(x, spl(x), 'r-',
    label='spline')
plt.plot(x, y_th, 'r--',
    label='theory')

# add knots
for knot in t:
    plt.axvline(knot, color='green', linestyle='--')

# fix axes, title and add legend
fig.suptitle('natural cubic splines (std. err = %.3f)' % SE, fontsize=18)
plt.xlabel('x', fontsize=14)
plt.ylabel('y', fontsize=14)

plt.legend()
plt.savefig("figures/ncs-example-dd.pdf")
plt.close()

noise = sns.distplot(noise);
noise.get_figure().savefig("figures/ncs-noise-dd.pdf")
