import seaborn as sns; sns.set()
import matplotlib.pyplot as plt
sns.set(font="DejaVu Sans") # or you don't get minus sign in pdf - weird

import numpy as np

N = 100
x = np.linspace(0,1,N)

# make some noise normally distributed
noise = np.random.normal(0, 0.15, size=N)

n=np.arange(0,4)
print(n)
n = np.append(n, np.arange(5,9))
print(n)
print(n.reshape(2,4))

print(n.reshape(2,4)[:,0])

cR = 60
cC = 40
arr = np.empty((0,cC), int)
for k in np.arange(0,cR):
#    arr = np.append(arr, np.arange(cC*k,cC*(k+1)))
    arr = np.append(arr, np.random.normal(0, 0.15, size=cC))
print(arr)
out = arr.reshape(cR,cC)
print(out)
print(out.shape)

print(out.mean(axis=0))
print(out.std(axis=0))

print(out[:,0].mean())
print(out[:,0].std())

x = np.linspace(0,1,cC)
import pandas as pd
pout = pd.DataFrame(np.vstack((x,out)).T)
print(pout)
print(x)
print(pd.melt(pout, [0]))

sc = sns.scatterplot(x=0, y='value', hue='variable',
             data=pd.melt(pout, [0]))

m=out.mean(axis=0)
s=out.std(axis=0)
sns.lineplot(x,10*m, ax=sc)
print(m)
plt.errorbar(x = x, y = 10*m, yerr = 10*s, capsize=.2)
#plt.errorbar(x, m, (m - s, m + s))

sc.get_figure().savefig("figures/noises.pdf")
