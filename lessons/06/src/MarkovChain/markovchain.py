import numpy as np

class MarkovChain():
    def __init__(self, transition_matrix, states):
        """
        """
        self.transition_matrix = np.atleast_2d(transition_matrix)
        self.states = states
        self.rng = np.random.default_rng()

    def next_state(self, current_state):
        """
        """
        return self.rng.choice(
            self.states,
            p = self.transition_matrix[self.states.index(current_state),:]
        )

    def generate_states(self, current_state, no=10):
        """
        """
        future_states = []
        for i in range(no):
            next_state = self.next_state(current_state)
            future_states.append(next_state)
            current_state = next_state
        return future_states

    def __repr__(self):
        """
        """
        rep = "\t|".join([""] + self.states)
        for s in self.states:
            rep += "\n"
            vals=[str(a) for a in self.transition_matrix[self.states.index(s),:].tolist()]
            rep += "\t|".join([s]+vals)

        return rep

states = [  'Sunny', 'Rainy',   'Snowy']
TM = [   [  0.8,      0.19,     0.01],
         [  0.2,      0.7,      0.1],
         [  0.1,      0.2,      0.7]
]

Weather = MarkovChain(TM,states)
print(Weather)

print("Sunny ==>",Weather.next_state('Sunny'))

print("Snowy ==>",Weather.next_state('Snowy'))

chain = Weather.generate_states('Sunny',10000)
#print(chain)
print(dict([[x,chain.count(x)/len(chain)] for x in states]))
