%%%%%%%%%
%%\begin{frame}[allowframebreaks]{\insertsection: \insertsubsection}
\begin{frame}{\insertsection: \insertsubsection}
%%
%%%%
\Large
\begin{itemize}

\item A Markov chain describes the evolution of a system that hops from one state to another.
\pause
\item The probability of transition at each step depends only on the current state.
\pause
\item Given a succession of states $\{X_k\}_1^n$ the conditional probability of state $x$ is
\begin{equation*}
  Pr(X_{n+1}=x|X_1=x_1,\ldots,X_n=x_n)=Pr(X_{n+1}=x|X_n=x_n)
\end{equation*}
\pause
\item The system is memoryless.
\pause
\item Widely employed in economics, game theory, communication theory, genetics and finance.
\pause
\item The algorithm known as PageRank, which was originally proposed for the internet search engine Google, is based on a Markov process.
\end{itemize}

\end{frame}

\begin{frame}{\insertsection: \insertsubsection}
  \Large
  \begin{itemize}
    \item The possible values of $X_i$ form a countable set S are called the state space of the chain.
    \pause
    \item The probabilities associated with various state changes are called transition probabilities.
    \pause
    \item If the Markov chain has N possible states, then the transition matrix  will be an N x N matrix, such that entry (I, J) is the probability of transitioning from state I to state J.
    \pause
    \item Additionally, the transition matrix must be a stochastic matrix, a matrix whose entries in each row must add up to exactly 1.
    \pause
    \item  Since each row represents its own probability distribution.
  \end{itemize}
\end{frame}

\begin{frame}{\insertsection: \insertsubsection}
  \sltitle{Simple example: weather}
  \Large
  \begin{itemize}
    \item State space : Sunny, Rainy, Snowy
    \pause
    \item Transition Matrix :
    %\rowcolors{1}{RowColorOdd}{RowColorEven}%
    \begin{tabular}{>{\columncolor{gray!40}}l|l|l|l}
\rowcolor{gray!40}      &Sunny	&Rainy	&Snowy \\ \hline
\rowcolor{RowColorOdd}Sunny	&0.8	&0.19	&0.01\\
\rowcolor{RowColorEven}Rainy	&0.2	&0.7	&0.1\\
\rowcolor{RowColorOdd}Snowy	&0.1	&0.2	&0.7\\
    \end{tabular}

  \end{itemize}
\end{frame}

\begin{frame}{\insertsection: \insertsubsection}
\begin{center}
  \includegraphics[width=\textwidth]{weather-markovchain.png}
\end{center}
%%%%%%%%%
\end{frame}

\begin{frame}[fragile]{\insertsection: \insertsubsection}
\begin{lstlisting}[basicstyle=\small,backgroundcolor=\color{codegray}]
numpy.random.Generator.choice
method
Generator.choice()
    choice(a, size=None, replace=True, p=None, axis=0):
    Generates a random sample from a given 1-D array
    Parameters:
    a : 1-D array-like or int
        If an ndarray, a random sample is generated from its
        elements. If an int, the random sample is generated as
        if a were np.arange(a)
    size : int or tuple of ints, optional
        Output shape. If the given shape is, e.g., (m, n, k),
        then m * n * k samples are drawn from the 1-d a.
        If a has more than one dimension, the size shape will be
        inserted into the axis dimension, so the output ndim
        will be a.ndim - 1 + len(size).
        Default is None, in which case a single value is returned.
    replace : boolean, optional
        Whether the sample is with or without replacement
    p : 1-D array-like, optional
        The probabilities associated with each entry in a.
        If not given the sample assumes a uniform distribution
        over all entries in a.
    axis : int, optional
        The axis along which the selection is performed. The default, 0, selects by row.
    shuffle : boolean, optional
        Whether the sample is shuffled when sampling without replacement. Default is True, False provides a speedup.

    Returns:
    samples : single item or ndarray
        The generated random samples
    Raises:
        ValueError
\end{lstlisting}
\end{frame}

\begin{frame}{\insertsection: \insertsubsection}
\includenotebook[1]{350}{600}{markovchain.pdf}
\end{frame}

\begin{frame}{\insertsection: \insertsubsection}
  \includenotebook[1]{220}{350}{markovchain.pdf}
\end{frame}

\begin{frame}[fragile]{\insertsection: \insertsubsection}
  \includenotebook[2]{550}{670}{markovchain.pdf}
  \includenotebook[2]{420}{490}{markovchain.pdf}
\begin{lstlisting}[basicstyle=\small,backgroundcolor=\color{codegray}]
      |Sunny	|Rainy	|Snowy
Sunny	|0.8	|0.19	|0.01
Rainy	|0.2	|0.7	|0.1
Snowy	|0.1	|0.2	|0.7
Sunny ==> Rainy
Snowy ==> Snowy
{'Sunny': 0.464, 'Rainy': 0.3965, 'Snowy': 0.1395}
\end{lstlisting}
\end{frame}

\begin{frame}{\insertsection: \insertsubsection}
  \Large
  Some important properties of Markov chains:
  \begin{itemize}
    \item \textbf{Reducibility}: a Markov chain is said to be irreducible if it is possible to get to any state from any state. In other words, a Markov chain is irreducible if there exists a chain of steps between any two states that has positive probability.
    \pause
    \item \textbf{Periodicity}: a state in a Markov chain is periodic if the chain can return to the state only at multiples of some integer larger than 1. Thus, starting in state $i$, the chain can return to $i$ only at multiples of the period $k$, and $k$ is the largest such integer. State $i$ is aperiodic if $k = 1$ and periodic if
    $k > 1$.
    \pause
    \item \textbf{Transience and Recurrence}: A state $i$ is said to be transient if, given that we start in state $i$, there is a non-zero probability that we will never return to $i$. State $i$ is recurrent (or persistent) if it is not transient. A recurrent state is known as positive recurrent if it is expected to return within a finite number of steps and null recurrent otherwise.

\end{itemize}
  \end{frame}
\begin{frame}{\insertsection: \insertsubsection}
  \Large
  Some important properties of Markov chains:
  \begin{itemize}
    \item \textbf{Ergodicity}: a state $i$ is said to be ergodic if it is aperiodic and positive recurrent. If all states in an irreducible Markov chain are ergodic, then the chain is said to be ergodic.
    \pause
    \item \textbf{Absorbing State}: a state $i$ is called absorbing if it is impossible to leave this state. Therefore, the state $i$ is absorbing if
    %$p_{ii} = 1$ and $p_{ij} = 0$ for $i \neq j$.
    \begin{align}
        p_{ii} &= 1\\
        p_{ij} &= 0, \text{for} \: i \neq j
    \end{align}
    If every state can reach an absorbing state, then the Markov chain is an absorbing Markov chain.
\end{itemize}
  \end{frame}
