
\begin{frame}{\insertsection: \insertsubsection}
  \sltitle{Definition}
  \Large
Non-parametric statistics may be viewed as the collection of statistical methods that
\begin{columns}[t]%
\begin{column}[t]{0.7\textwidth}
%
  \begin{itemize}
    \item \textbf{either} do not relate to specific parameters \\(the broad definition)
    \item \textbf{or} maintain their distributional properties irrespective of the underlying distribution of the data (distribution-free methods).
  \end{itemize}
\end{column}
\end{columns}
\end{frame}

\begin{frame}{\insertsection: \insertsubsection}
  \sltitle{Why use non-parametric statistics}
  \Large
The parameters which \textit{non-parametric} methods specifically
\textbf{don't use} are the parameters of the normal distribution:
\begin{center}
the \textit{mean} and the \textit{standard deviation}.
\end{center}

Non-parametric methods must be used in the following cases:
\begin{itemize}
  \item The measurement scale of the data is ordinal rather than
  interval or ratio.\\
  Only the relative position of data points is meaningful (arithmetic operation are not applicable)
  \item The measurements are on interval or ratio scales but the frequency
  distribution shows a marked departure from the normal distribution.
\end{itemize}
\vskip 1em

As the mean has non relevance, tests are based on the \textit{median}.
\end{frame}
\begin{frame}{\insertsection: \insertsubsection}

  \Large
  %\sltitle{Non-parametric methods are based on \textit{rank} of the data values:}
  \begin{center}
    \textbf{Non-parametric methods are based on \textit{rank} of the data values:}
  \end{center}

  \begin{textblock*}{.5\textwidth}(2em,5em)
    \begin{block}{}
      \begin{itemize}
        \item each value is replaced by a number
          giving its place in the sequence
          from highest to lowest.
        \item the calculations are based only on the ranks
        \item with ordinal scale data, all meaningful information is in ranks
        \item with interval and ratio scales there is loss of information.
        %\item
        If $x_2$ had been $9300$, it's rank would still be $8$: so absolute scale is irrelevant and we loose information in case of
        interval or ratio data
      \end{itemize}
    \end{block}
  \end{textblock*}


  \begin{textblock*}{.3\textwidth}(21em,6em)
  \onslide<+->{
  \begin{block}{\centering use of ranks}
    \begin{align*}
          &   \mathrel{\stackrel{\makebox[0pt]{\mbox{\normalfont\tiny sort}}}{\Downarrow}}     &        \\
      x_1 & = 4.3, & R(x_1) = 5 \\
      x_2 & = 9.3, & R(x_2) = 8 \\
      x_3 & = 0.3, & R(x_3) = 1 \\
      x_4 & = 2.9, & R(x_4) = 3 \\
      x_5 & = 3.2, & R(x_5) = 4 \\
      x_6 & = 7.7, & R(x_6) = 7 \\
      x_7 & = 5.0, & R(x_7) = 6 \\
      x_8 & = 0.4, & R(x_8) = 2
    \end{align*}
  % block text
  % \begin{itemize} %%[leftmargin=*]
  % \item item
  % \end{itemize}
  \end{block}
  } %%onslide
  \end{textblock*}
  \begin{textblock*}{.3\textwidth}(21em,6em)
  \onslide<+->{
  \begin{block}{\centering use of ranks}
    \begin{align*}
          &   \mathrel{\stackrel{\makebox[0pt]{\mbox{\normalfont\tiny sort}}}{\Uparrow}}     &        \\
      x_3 & = 0.3, & R(x_3) = 1 \\
      x_8 & = 0.4, & R(x_8) = 2 \\
      x_4 & = 2.9, & R(x_4) = 3 \\
      x_5 & = 3.2, & R(x_5) = 4 \\
      x_1 & = 4.3, & R(x_1) = 5 \\
      x_7 & = 5.0, & R(x_7) = 6 \\
      x_6 & = 7.7, & R(x_6) = 7 \\
      x_2 & = 9.3, & R(x_2) = 8
    \end{align*}
  % block text
  % \begin{itemize} %%[leftmargin=*]
  % \item item
  % \end{itemize}
  \end{block}
  %}
  } %%onslide

  \end{textblock*}

\end{frame}
%
\subsection{Ranks}
\begin{frame}{\insertsection: \insertsubsection}
  \sltitle{Tied ranks}
\Large
\begin{textblock*}{.5\textwidth}(2em,9em)
    \begin{block}{}
      \begin{itemize}
        \item if there are two or more identical values, ranks are
          \textit{tied}.
        \item The rank is set to the average of the ranks of
          the group of values.
        \item if tied ranks occur, some test statistics may need
          an adjustment, but this is not usually crucial
      \end{itemize}
    \end{block}
  \end{textblock*}

  \begin{textblock*}{.3\textwidth}(21em,7em)
%  \onslide<+->{
  \begin{block}{\centering tied ranks}
    \begin{center}
    \begin{tabular}{cc}
      $x_i$ & $R(x_i)$\\ \hline
      56 & 7 \\
      \rowcolor{yellow!40}42 & 4 \\
      \rowcolor{green!40} 61 & 8.5 \\
      \rowcolor{green!40} 61 & 8.5 \\
      \rowcolor{yellow!40}42 & 4 \\
      55 & 6 \\
      35 & 1 \\
      \rowcolor{yellow!40}42 & 4 \\
      39 & 2 \\
      65 & 10
    \end{tabular}
  \end{center}
  \end{block}
%  } %%onslide
  \end{textblock*}

\end{frame}
\begin{frame}[fragile]{\insertsection: \insertsubsection}
%\Large
\sltitle{Computing ranks (matlab)}

\texttt{[R,TIEADJ] = tiedrank(X)} computes the ranks of the values in the vector \texttt{X}. If any \texttt{X} values are tied, \texttt{tiedrank} computes their average rank. The return value \texttt{TIEADJ} is an adjustment for ties required by the non-parametric tests \texttt{signrank} and \texttt{ranksum}, and for the computation of Spearman's rank correlation.\\

\begin{lstlisting}[basicstyle=\footnotesize,backgroundcolor=\color{codegray}]
>> data=[56 42 61 61 42 55 35 42 39 65]
data =

    56    42    61    61    42    55    35    42    39    65
>> tiedrank(data)
ans =

  Columns 1 through 7

    7.0000    4.0000    8.5000    8.5000    4.0000    6.0000    1.0000

  Columns 8 through 10

    4.0000    2.0000   10.0000

\end{lstlisting}

\end{frame}
%%
% Tied Observations
%
% The term tie is used in connection with rank order statistics. Tied observations are observations having the same value, which prohibits the assignment of unique rank numbers. As a way out tied observations are assigned to the average of their hypothetical ranks.
%
\subsection{Median}
\begin{frame}[fragile]{\insertsection: \insertsubsection}
  \sltitle{Compute the median}
  \Large
\begin{textblock*}{.48\textwidth}(\tblc,7em)
  Assuming $\{x_i\}_{i=1}^n$ is a list of $n$ rank\-able items,\\
  and $x_{[r]}$ is the $r^{th}$ ranked value of $x$\\
  \begin{equation*}
    med(x) = \begin{cases}
                  x_{[\frac{n+1}{2}]}, & n \quad \textrm{odd} \\
                  \frac{1}{2}(x_{[\frac{n}{2}]}+x_{[\frac{n}{2}+1]}), & n \quad \textrm{even}
                \end{cases}
  \end{equation*}
%  \vskip 1.5em
  \begin{lstlisting}[basicstyle=\footnotesize,backgroundcolor=\color{codegray}]
  >> median(data)
  ans =

     48.5000
  \end{lstlisting}
  \begin{align*}
      med(x) & = (r_{[5]}+r_{[6]})/2 \\
             & = (42+55)/2 = 48.5
  \end{align*}
\end{textblock*}

\begin{textblock*}{.3\textwidth}(21em,7em)
%  \onslide<+->{
\begin{block}{\centering tied ranks}
  \begin{center}
  \begin{tabular}{ccc}
    $x_i$ & $R(x_i)$ & $\overline{r}(x_i)$\\ \hline
    56 & 7 & \textit{7}\\
    \rowcolor{yellow!40}42 & 4 & \textit{3}\\
    \rowcolor{green!40} 61 & 8.5 & \textit{8}\\
    \rowcolor{green!40} 61 & 8.5&  \textit{9}\\
    \rowcolor{yellow!40}42 & 4 & \textit{4}\\
    55 & 6 & \textit{6}\\
    35 & 1 & \textit{1}\\
    \rowcolor{yellow!40}42 & 4 & \textit{5}\\
    39 & 2 & \textit{2}\\
    65 & 10 & \textit{10}
  \end{tabular}
\end{center}
\end{block}
%  } %%onslide
\end{textblock*}

\end{frame}
%
\subsection{Summary}
\begin{frame}{\insertsection: \insertsubsection}
  \Large
  \begin{itemize}
    \item Non-parametric tests make only mild assumptions about the data and are appropriate when the distribution of the data is non-normal. \\
\end{itemize}\begin{itemize}
    \item On the other hand, they are less powerful than classical methods for normally distributed data.
  \end{itemize}
\begin{textblock*}{\textwidth}(\tblc,11em)
  \begin{block}{\centering Comparison of parametric and non-parametric methods}
   \resizebox{\textwidth}{!}{%

    \begin{tabular}{p{4cm} p{4cm} p{4cm}}
      \hline
           & \multicolumn{2}{c}{Method}\\
      \multicolumn{1}{c}{Data} & \multicolumn{1}{c}{Parametric} & \multicolumn{1}{c}{Non-parametric} \\
      \hline
      Normal, large $n$ &
        Most precise and reliable, result possible
        & If $H_0$ rejected: result would be the same with parametric test\\
      Non-normal, small $n$ &
        Result completely unreliable: often leads to false rejection of $H_0$ &
        Best result possible with given quality of data\\
      \hline
    \end{tabular}
    }
  \end{block}
\end{textblock*}


\end{frame}
