# -*- coding: utf-8 -*-
import os, sys
import numpy as np
import pandas as pd

# cycler is a separate package extracted from matplotlib.
from cycler import cycler
import matplotlib.pyplot as plt
import seaborn as sns; sns.set()
sns.set(font="DejaVu Sans") # or you don't get minus sign in pdf - weird
def mkSignalPlot(t,s,title,tag):
    # init figure (with size optimized for slides)
    fig, ax = plt.subplots(figsize = (8,5))

    plt.plot(t,s, '-')

    fig.suptitle(title, fontsize=18)
    plt.xlabel('t (s)', fontsize=14)
    plt.ylabel('amp (a.u.)', fontsize=14)

    plt.savefig("figures/psd-signal-%s.pdf" % tag)
    plt.close()

def mkSignal(len, samp, amps=None, freqs=None, noiseLevel=1):
    # create time axis
    t = np.linspace(0,len,num=samp*len, endpoint=False)
    # create zero mean normal noise - same size
    s = np.random.normal(0, noiseLevel, size=samp*len)
    # stack coefficients if any
    if amps and freqs:
        ak = np.array(amps, ndmin=2).T
        fk = np.array(freqs, ndmin=2).T
        ak = ak / np.sum(ak)
        #
        coef = np.hstack([ak,fk])
        for c in coef:
            s = np.add(s,c[0]*np.sin(2*np.pi*c[1]*t))

    return t,s

    # / np.sqrt(np.sum(s**2))

def mkPowerPlot(f,P,fs,title,tag):
    # init figure (with size optimized for slides)
    fig, ax = plt.subplots(figsize = (8,5))
    plt.loglog(f, P)

    fig.suptitle(title, fontsize=18)
    plt.xlabel('f (Hz)', fontsize=14)
    plt.ylabel('PSD ($V^2/Hz$)', fontsize=14)
    plt.grid(True, which="major", ls="-")
    plt.grid(True, which="minor", ls="--")

    plt.xlim(0.1,fs)
    plt.ylim(.5e-2,.5)

    out="figures/psd-%s.pdf" % tag
    plt.savefig(out)
    print("mkPowerPlot() ==> %s" % out)
    plt.close()

import pandas as pd
def storeSignal(t,s,out):
    pd.DataFrame(np.vstack([t,s]).T,columns=['t','s']).to_csv(out, index = False)

sps = 100 # samples per second
len =   100 # s
amps =  [1,1,1,1]
freqs = [0.3,0.5,1,10]
#
noise = 1

t,s = mkSignal(len,sps,amps,freqs,0)
mkSignalPlot(t,s,'signal (%g sps) noise   0%%' % sps,'nonoise')

# from scipy import signal
# NFFT=2048
# N=s.shape[0]
# stderr=1./np.sqrt(N/NFFT)
# f, Pxx_den = signal.welch(s, sps, nperseg=NFFT)
# mkPowerPlot(f,Pxx_den,f[-1],
#     #    '$\Delta f$ = %g Hz ($f_s$ = %g Hz), NFFT=%d, noise 100%%' % (f[1],f[-1],NFFT),
#         'NFFT=%4d, $\Delta f$ = %.2f Hz, stderr=%.2f - nonoise' % (NFFT,f[1],stderr),
#         'nonoise-%d' % NFFT)
# #sys.exit()

t,s = mkSignal(len,sps,amps,freqs,noise)
mkSignalPlot(t,s,'signal (%g sps) noise 100%%' % sps,'noise')
storeSignal(t,s,'data/psd-signal-noise.csv')

from scipy import signal

N=s.shape[0]
for NFFT in [256, 512, 1024, 2048]:
    f, Pxx_den = signal.welch(s, sps, nperseg=NFFT)

    stderr=1./np.sqrt(N/NFFT)
    print('NFFT=',NFFT,'delta f=',f[1],'fs=',f[-1],'stderr=',stderr)
    mkPowerPlot(f,Pxx_den,f[-1],
        'NFFT=%4d, $\Delta f$ = %.2f Hz, stderr=%.2f' % (NFFT,f[1],stderr),
        'noise-%d' % NFFT)
#
# # cycler is a separate package extracted from matplotlib.
# from cycler import cycler
# import matplotlib.pyplot as plt
# import seaborn as sns; sns.set()
# sns.set(font="DejaVu Sans") # or you don't get minus sign in pdf - weird
# # init figure (with size optimized for slides)
# fig, ax = plt.subplots(figsize = (8,5))
#
# plt.plot(t,s, '-')
#
# fig.suptitle('signal (%g sps) no noise' % sps, fontsize=18)
# plt.xlabel('t (s)', fontsize=14)
# plt.ylabel('amp (a.u.)', fontsize=14)
#
# plt.savefig("figures/psd-signal-nonoise.pdf")
# plt.close()
#
# t,s = mkSignal(len,sps,amps,freqs,noise)
# plt.plot(t,s, '-')
# fig.suptitle('signal (%g sps) noise' % sps, fontsize=18)
# plt.savefig("figures/psd-signal-noise.pdf")
# plt.close()

t,s = mkSignal(len,sps,None,None,1)
mkSignalPlot(t,s,'signal (%g sps) only noise' % sps,'onlynoise')
N=s.shape[0]
NFFT=2048
f, Pxx_den = signal.welch(s, sps, nperseg=NFFT)

stderr=1./np.sqrt(N/NFFT)
print('NFFT=',NFFT,'delta f=',f[1],'fs=',f[-1],'stderr=',stderr)
mkPowerPlot(f,Pxx_den,f[-1],
    'NFFT=%4d, $\Delta f$ = %.2f Hz, stderr=%.2f' % (NFFT,f[1],stderr),
    'onlynoise-%d' % NFFT)
