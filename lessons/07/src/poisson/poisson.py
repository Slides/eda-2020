# -*- coding: utf-8 -*-

# cycler is a separate package extracted from matplotlib.
from cycler import cycler
import matplotlib.pyplot as plt
import seaborn as sns; sns.set()
sns.set(font="DejaVu Sans") # or you don't get minus sign in pdf - weird
from matplotlib.ticker import MaxNLocator

import numpy as np
from scipy.stats import poisson

# init figure (with size optimized for slides)
fig, ax = plt.subplots(figsize = (8,5))

mu = [1, 4, 10]
N = 20
x = np.linspace(0,N,N+1)
print(x)

for m in mu:
    plt.plot(x, poisson.pmf(x, m), '-o', ms=6, label='$\lambda$ = %d' % m)

fig.suptitle('Poisson : probability mass function', fontsize=18)
plt.xlabel('k', fontsize=14)
plt.ylabel('P(X=k)', fontsize=14)
plt.legend()
ax.xaxis.set_major_locator(MaxNLocator(integer=True))
plt.savefig("figures/poisson.pdf")

plt.close()

fig, ax = plt.subplots(figsize = (8,5))
for m in mu:
    plt.plot(x, np.cumsum(poisson.pmf(x, m)), '-o', ms=6, label='$\lambda$ = %d' % m)

fig.suptitle('Poisson : cumulative distribution function', fontsize=18)
plt.xlabel('k', fontsize=14)
plt.ylabel('P(X$\leq$k)', fontsize=14)
plt.legend()
ax.xaxis.set_major_locator(MaxNLocator(integer=True))
plt.savefig("figures/poisson-cum.pdf")
