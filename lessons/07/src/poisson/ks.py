# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd

def getLayers(inFile, T=1):
    data = pd.read_csv(inFile)

    # return first column as nparray
    return data.iloc[:, 0].to_numpy()/T

layers = getLayers('data/strat.dat',45)

from scipy.stats import kstest
test = kstest(layers, 'uniform')

print('ks: uniform', test, test.pvalue)
alpha = 0.05
# For a significance level of 5%, if the p-value falls lower than 5%,
# the null hypothesis is invalidated.
if test.pvalue > alpha:
	print('fail to reject H0 p=%.3f' % test.pvalue)
else:
	print('reject H0 p=%.3f' % test.pvalue)


# cycler is a separate package extracted from matplotlib.
from cycler import cycler
import matplotlib.pyplot as plt
import seaborn as sns; sns.set()
sns.set(font="DejaVu Sans") # or you don't get minus sign in pdf - weird

cnl = layers.shape[0]

layers = np.append(layers, 1.)
uni = np.array([ (v,v) for v in layers]).flatten()
pep = np.array([(idx[0]/cnl,(idx[0]+1)/cnl) for idx, val in np.ndenumerate(layers)]).flatten()

# init figure (with size optimized for slides)
fig, ax = plt.subplots(figsize = (8,5))

plt.plot(layers,layers, '-o', ms=6)
plt.plot(uni[:-1],pep[:-1],'-o', ms=6)

fig.suptitle('Kolmogorov-Smirnov test for uniformity', fontsize=18)
plt.xlabel('$t_i/T$ (proportion of sequence passed)', fontsize=14)
plt.ylabel('Proportion of events passed', fontsize=14)

plt.savefig("figures/kstest.pdf")
