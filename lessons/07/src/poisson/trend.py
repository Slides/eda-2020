# -*- coding: utf-8 -*-
import os, sys
import pandas as pd
import numpy as np

def getLayers(inFile):
    layers = pd.read_csv(inFile)

    return layers.diff().dropna(), layers.shape[0]

lengths, cLayers = getLayers('data/strat.dat')
print('Number of events =', cLayers)
print('Lengths : ')
print(lengths)

#Spearman's rank correlation
from scipy.stats import spearmanr

coef, p = spearmanr(lengths.index, lengths['depth'])
print('Spearmans correlation coefficient: %.3f' % coef)
# interpret the significance
alpha = 0.05
if p > alpha:
	print('Samples are uncorrelated (fail to reject H0) p=%.3f' % p)
else:
	print('Samples are correlated (reject H0) p=%.3f' % p)

# let's plot it - traditional matplotlib way
import matplotlib.pyplot as plt
import seaborn as sns; sns.set()
sns.set(font="DejaVu Sans") # or you don't get minus sign in pdf - weird
# init figure (with size optimized for slides)
fig, ax = plt.subplots(figsize = (8,5))

# plot results
plt.plot(lengths.index, lengths['depth'], 'o',
    markerfacecolor='blue', markersize=6, color='skyblue',
    label='p=%.3f' % p) #, linewidth=4)

# fix axes, title and add legend
fig.suptitle('Tuff layers : Spearmans correlation coefficient: %.3f' % coef, fontsize=18)
plt.xlabel('Interval number', fontsize=14)
plt.ylabel('Interval length (m)', fontsize=14)

plt.legend()
plt.savefig("figures/trend.pdf")

# let's plot it using pandas methods
# pandas.DataFrame.plot
# Make plots of DataFrame using matplotlib / pylab.
# New in version 0.17.0: Each plot kind has a corresponding method
# on the DataFrame.plot accessor:
# df.plot(kind='line') is equivalent to df.plot.line().

p = lengths.reset_index().plot.scatter(
    x='index', y='depth',use_index=True,
    figsize = (8,5),
    title='Tuff layers : Spearmans correlation coefficient: %.3f' % coef
    )

# eventually set other properties

p.figure.savefig('figures/trend-pd.pdf')

lengths["Rank"] = lengths["depth"].rank()
print(lengths)
