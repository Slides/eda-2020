# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import seaborn as sns; sns.set()
sns.set(font="DejaVu Sans") # or you don't get minus sign in pdf - weird

def addLine(y, data, label, dy=0.1, off=2, xlab=0.8):
    yoff=off*dy
    plt.plot(data, y*np.ones(data.shape[0]), 'o',
        markerfacecolor='blue', markersize=6, color='skyblue',
        label=label) #, linewidth=4)
    plt.hlines(y, 0, 1, colors='b')
    plt.vlines(0, y-dy, y+dy, colors='b')
    plt.vlines(1, y-dy, y+dy, colors='b')
    plt.annotate(label,(xlab,y+yoff))

import numpy as np
N = 10

# init figure (with size optimized for slides)
fig, ax = plt.subplots(figsize = (8,5))
ax.set_yticklabels([])
ax.set_xticklabels([])

# plot results
plt.ylim(0, 6)
plt.xlim(0,1)

even = np.linspace(0,1,N+1)
print(even[1:-1])
addLine(5,even[1:-1],'Evenly spaced')

#from numpy.random import rand
rnd = np.random.rand(N)
print(rnd)
addLine(4,rnd,'Random')

clu = np.array([0.25866817, 0.79519111, 0.64895204, 0.9078173,  0.92080031, 0.8001797,
 0.80545478, 0.42369415, 0.90329329, 0.50032853])
print(clu)
addLine(3, 4*(clu-0.75), 'Clustered')

trend = np.array([
    0.04,0.05,
    0.11,0.14,0.15,
    0.31,0.33,0.34,
    0.6,0.62,
    0.95])
addLine(2, trend, 'Trend')

patrn = np.array([
    0.09,0.11,
    0.29,0.31,
    0.45,0.4575,0.47,
    0.75,0.73,
    0.89,0.91
])
addLine(1,patrn,'Pattern')
plt.savefig("figures/eventdist.pdf")
#
# # Load the example mpg dataset
# mpg = sns.load_dataset("mpg")
#
# # Plot miles per gallon against horsepower with other semantics
# scat = sns.relplot(x="horsepower", y="mpg", hue="origin", size="weight",
#             sizes=(40, 400), alpha=.5, palette="muted",
#             height=6, data=mpg)
# plt.savefig("figures/eventdist.pdf")
