# -*- coding: utf-8 -*-
import os, sys
import numpy as np
import pandas as pd

# cycler is a separate package extracted from matplotlib.
from cycler import cycler
import matplotlib.pyplot as plt
import seaborn as sns; sns.set()
sns.set(font="DejaVu Sans") # or you don't get minus sign in pdf - weird

#https://scipy-cookbook.readthedocs.io/items/SignalSmooth.html
def smooth(x,window_len=5,window='hanning'):
	"""smooth the data using a window with requested size.

	This method is based on the convolution of a scaled window with the signal.
	The signal is prepared by introducing reflected copies of the signal
	(with the window size) in both ends so that transient parts are minimized
	in the begining and end part of the output signal.

	input:
    	x: the input signal
    	window_len: the dimension of the smoothing window; should be an odd integer
    	window: the type of window from 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'
        	flat window will produce a moving average smoothing.

	output:
    	the smoothed signal

	example:

	t=linspace(-2,2,0.1)
	x=sin(t)+randn(len(t))*0.1
	y=smooth(x)

	see also:

	numpy.hanning, numpy.hamming, numpy.bartlett, numpy.blackman, numpy.convolve
	scipy.signal.lfilter

	TODO: the window parameter could be the window itself if an array instead of a string
	NOTE: length(output) != length(input), to correct this: return y[(window_len/2-1):-(window_len/2)] instead of just y.
	"""

	if x.ndim != 1:
		raise ValueError("smooth only accepts 1 dimension arrays.")

	if x.size < window_len:
		raise ValueError("Input vector needs to be bigger than window size.")


	if window_len<3:
		return x


	if not window in ['flat', 'hanning', 'hamming', 'bartlett', 'blackman']:
		raise ValueError("Window MUST be one of 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'")


	s=np.r_[x[window_len-1:0:-1],x,x[-2:-window_len-1:-1]]
	#print(len(s))
	if window == 'flat': #moving average
		w=np.ones(window_len,'d')
	else:
		w=eval('np.'+window+'(window_len)')

	y=np.convolve(w/w.sum(),s,mode='valid')
	#print(len(y))
	yw = int(window_len/2)
	return y[yw:-yw]

def getData():
    raw_url="https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-andamento-nazionale/dpc-covid19-ita-andamento-nazionale.csv"
    raw_url="data/dpc-covid19-ita-andamento-nazionale.csv" #updated 2020-06-25
    covid=pd.read_csv(raw_url, error_bad_lines=False)
    #print(covid.info())
    #return covid['totale_positivi'].to_numpy()
    return covid['nuovi_positivi'].to_numpy()

t_pos = getData()
days = np.linspace(0,t_pos.shape[0],t_pos.shape[0], endpoint=False)

# init figure (with size optimized for slides)
fig, ax = plt.subplots(figsize = (8,5))

plt.plot(days,t_pos, '-o', ms=3)

fig.suptitle('COVID-19 Italy : daily positives', fontsize=18)
plt.xlabel('t (days)', fontsize=14)
plt.ylabel('N', fontsize=14)

plt.savefig("figures/smooth-nuovi-positivi.pdf")
plt.close()

w = 7 # days
t_pos_s = smooth(t_pos) #x,window_len=5,window='hanning')
t_pos_sw = smooth(t_pos,w)
# init figure (with size optimized for slides)
fig, ax = plt.subplots(figsize = (8,5))

plt.plot(days,t_pos, '--o', ms=3, label='raw')
plt.plot(days,t_pos_s, '-', ms=3, label='5 days')
plt.plot(days,t_pos_sw, '-', ms=3, label='%d days' % w)


fig.suptitle('COVID-19 Italy : daily positives', fontsize=18)
plt.xlabel('t (days)', fontsize=14)
plt.ylabel('N', fontsize=14)
plt.legend()

plt.savefig("figures/smooth-nuovi-positivi-s.pdf")
plt.close()

from scipy import interpolate
days_i = np.linspace(days[0],days[-1], 4*(len(days)-1)+1, endpoint=True)

int_f = interpolate.interp1d(days, t_pos_sw, kind="cubic")

# init figure (with size optimized for slides)
fig, ax = plt.subplots(figsize = (8,5))

plt.plot(days,t_pos, '--o', ms=3, label='raw')
plt.plot(days,t_pos_s, '-', ms=3, label='%d days' % w)
plt.plot(days_i,int_f(days_i), '-', ms=3, label='interp (4)')

fig.suptitle('COVID-19 Italy : daily positives', fontsize=18)
plt.xlabel('t (days)', fontsize=14)
plt.ylabel('N', fontsize=14)
plt.legend()

plt.savefig("figures/smooth-nuovi-positivi-si.pdf")
plt.close()


from scipy import signal
f_raw, P_raw = signal.welch(t_pos, 1, nperseg=32)
# init figure (with size optimized for slides)
fig, ax = plt.subplots(figsize = (8,5))
plt.loglog(f_raw, P_raw, '-o', ms=3, label='raw (NFFT=32)')

#plt.axvline(0.142857)
#plt.text(0.15287,1e5,'1 week',rotation=90)

fig.suptitle('COVID-19 Italy : daily positives', fontsize=18)
plt.xlabel('f (1/days)', fontsize=14)
plt.ylabel('PSD (a.u.)', fontsize=14)
plt.grid(True, which="major", ls="-")
plt.grid(True, which="minor", ls="--")

#plt.xlim(0.1,fs)
#plt.ylim(.5e-2,.5)

plt.legend()
out="figures/smooth-raw-psd.pdf"
plt.savefig(out)
print("mkPowerPlot() ==> %s" % out)
plt.close()

# init figure (with size optimized for slides)
fig, ax = plt.subplots(figsize = (8,5))
plt.loglog(f_raw, P_raw, '-o', ms=3, label='raw (NFFT=32)')

plt.axvline(0.142857)
plt.text(0.15287,1e5,'1 week',rotation=90)

fig.suptitle('COVID-19 Italy : daily positives', fontsize=18)
plt.xlabel('f (1/days)', fontsize=14)
plt.ylabel('PSD (a.u.)', fontsize=14)
plt.grid(True, which="major", ls="-")
plt.grid(True, which="minor", ls="--")

#plt.xlim(0.1,fs)
#plt.ylim(.5e-2,.5)

plt.legend()
out="figures/smooth-raw-psd-week.pdf"
plt.savefig(out)
print("mkPowerPlot() ==> %s" % out)
plt.close()

f_int, P_int = signal.welch(int_f(days_i), 4., nperseg=256)
# init figure (with size optimized for slides)
fig, ax = plt.subplots(figsize = (8,5))
plt.loglog(f_raw, P_raw, label='raw (NFFT=32)')
plt.loglog(f_int, P_int, label='s&i (NFFT=256)')

fig.suptitle('COVID-19 Italy : daily positives', fontsize=18)
plt.xlabel('f (1/days)', fontsize=14)
plt.ylabel('PSD (a.u.)', fontsize=14)
plt.grid(True, which="major", ls="-")
plt.grid(True, which="minor", ls="--")

#plt.xlim(0.1,fs)
#plt.ylim(.5e-2,.5)
plt.axvline(0.142857)
plt.text(0.15287,1e-7,'1 week',rotation=90)

plt.legend()
out="figures/smooth-raw-psd-int.pdf"
plt.savefig(out)
print("mkPowerPlot() ==> %s" % out)
plt.close()
