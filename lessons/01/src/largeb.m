fprintf("low b\n");
p=[3 -2 -4];
disp(p);
r=roots(p).';
R=classic(p);
N=noro(p);

fprintf('R-r = (%g,%g) N-r = (%g,%g)\n',R(1)-r(1),R(2)-r(2),N(1)-r(1),N(2)-r(2));

fprintf("\n\nhigh b\n");
p=[3 -20000 -4];disp(p);
r=roots(p).';
R=classic(p);
N=noro(p);

fprintf('R-r = (%g,%g) N-r = (%g,%g)\n',R(1)-r(1),R(2)-r(2),N(1)-r(1),N(2)-r(2));

function [ r ] = classic(p)
	delta = sqrt(p(2)^2-4*p(1)*p(3));
	
	r = [(-p(2)+delta)/(2*p(1)), (-p(2)-delta)/(2*p(1))];
end

function [ r ] = noro(p)
	q = -0.5*(p(2)+sign(p(2))*sqrt(p(2)^2-4*p(1)*p(3)));

	r = [q/p(1), p(3)/q];
end
