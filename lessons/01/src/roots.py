import numpy as np
import math

def classic(c):
	delta = math.sqrt(c[1]**2-4*c[0]*c[2])
	
	return [(-c[1]+delta)/(2*c[0]), (-c[1]-delta)/(2*c[0])];

def noro(c):
	q = -0.5*(c[1]+np.sign(c[1])*math.sqrt(c[1]**2-4*c[0]*c[2]))
	
	return [q/c[0], c[2]/q]

coeff = [3,-2,-4]
r=np.roots(coeff)
print("coeff = ",coeff)
print("roots = \t",r)

R=classic(coeff)
print("classic = \t",R,"(",R[0]-r[0],R[1]-r[1],")")

N=noro(coeff)
print("noro = \t\t",N,"(",N[0]-r[0],N[1]-r[1],")")

coeff = [3,-20000,-4]
r=np.roots(coeff)
print("coeff = ",coeff)
print("roots = \t",r)

R=classic(coeff)
print("classic = \t",R,"(",R[0]-r[0],R[1]-r[1],")")

N=noro(coeff)
print("noro = \t\t",N,"(",N[0]-r[0],N[1]-r[1],")")
