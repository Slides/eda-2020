%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[allowframebreaks,fragile]{\insertsection: \insertsubsection\ - tmux} %\insertsubsubsection}
%%\sltitle{\insertsubsubsection}
%\sltitle{\insertsubsection}
%% https://www.linuxtrainingacademy.com/tmux-tutorial/
\Large
\sltitle{What is tmux?}
\begin{itemize}
  \item \code{tmux} is a terminal multiplexer that allows you to manage multiple terminal sessions with a single terminal window, and you can easily switch between those multiple terminal sessions.

  \item In traditional UNIX workstation used to be \code{screen}.
\end{itemize}

\sltitle{Why use tmux?}

\begin{itemize}
  \item The power of \code{tmux} is that you can detach, or leave, or exit \code{tmux}, while all those terminal
sessions that are managed by \code{tmux} are still running in the background.

Then you can come back at a later time and reattach to those terminal sessions. If you work on remote systems, then \code{tmux} can be a lifesaver.

  \item For example, you can log into a remote system, start \code{tmux}, then start as many terminals as you need. If you lose your network connection, you won’t lose the work in those terminals, as they are still running on the server. You can simply SSH back in and reattach to those terminals.

  \item \code{tmux} doesn’t have to just protect you from accidental disconnections. You can use \code{tmux} to start long-running jobs on a remote system, then disconnect and come back later to see the results, or even scroll back through the output of each of those terminal sessions.

  \item By the way, you don’t miss anything while you’re gone. For example, let’s say you’re working from your laptop in the airport, and you have to board your flight in the next few minutes, meaning you have to close your laptop and disconnect from the internet.

  \item Normally if you wanted to start a command on a remote system that would take more than
the time you can stay connected, then you would be out of luck as that command would
get terminated when you disconnect. However, with \code{tmux} you can start the command, board your flight, and when you’re back on the network again see the results of your command, knowing it had run while you were away.
\end{itemize}

\sltitle{Creating tmux sessions}
\begin{itemize}
  \item The simplest way to use \code{tmux} is to run the \code{tmux} command.
\begin{minted}[bgcolor=minterm,fontsize=\normalsize]{bash}
tmux
\end{minted}

  \item This starts a new tmux session with a single window, or single terminal session inside of tmux, and displays a status line at the bottom of the screen. The status line displays the session name, which in this case is “0”.

  Next, you’ll see a list of windows. In this case, we only have one window represented by 0:bash*, with “0” being the windows index, “bash” being the windows name, and the asterisk representing that it is the current window.

  On the right-hand side of the status bar you’ll see the host name, time, and date.

  \includegraphics[width=.9\textwidth]{tmux-terminal.png}

\end{itemize}
%%\pagefigure{tmux-terminal.png}
\begin{itemize}
  \item From here you can run any commands like you would in any other terminal. When the last window, or terminal, that \code{tmux} is controlling closes, \code{tmux} exits and deletes the session.

  Since we only have one window slash terminal that \code{tmux} is controlling, when we type exit, it exits that window and thus exits \code{tmux} as well.
\begin{minted}[bgcolor=minterm,fontsize=\normalsize]{bash}
exit
\end{minted}
  \item If you want to give your session a meaningful name, then run
  \code{tmux new -s session\_name}. For example, let’s say we want to call this session “backups” because we are going to use it to manually run some backups. This way, if we have multiple tmux sessions, we’ll know that the one named “backups” is for backups.

  By the way, “new” is actually short for “new-session”. So, if you see “tmux new-session”, it’s the exact same thing as “tmux new”.
\begin{minted}[bgcolor=minterm,fontsize=\normalsize]{bash}
tmux new -s backups
\end{minted}
  \item Now you’ll notice that the session name is “backups” in the status line. Again, you can start using this terminal like you would any other terminal. Let’s pretend we want to run a backup command for multiple servers, so we want a new window or new terminal for each one of them.
\end{itemize}

\sltitle{Creating new tmux windows}

\begin{itemize}
  \item To create a new window, type \code{Ctrl-b c}.

  Hold down the \code{Ctrl} key, then type “b”, then release the \code{Ctrl} key, and finally type “c”.

\begin{minted}[bgcolor=minterm,fontsize=\normalsize]{bash}
Ctrl-b c
\end{minted}

  \item \code{Ctrl-b} is known as a prefix key in \code{tmux}. When you want to run a tmux command such as creating a new window, switching between windows, detaching from a session, etc., you’ll start out by typing \code{Ctrl-b}.

  \item In the status line, you’ll now see that we have two windows, “0:bash” and “1:bash*”. The asterisk beside “1:bash” means that anything we type will be sent to this window with an index of 1.

  If we type Ctrl-b c again, we’ll have three windows.
\end{itemize}
\sltitle{Switching between tmux windows}

\begin{itemize}
  \item To switch to the next window, use Ctrl-b n. If you are controlling the last window, or the last window has your focus, then the next window will wrap around to the first window, which is exactly what has happened here.

\begin{minted}[bgcolor=minterm,fontsize=\normalsize]{bash}
Ctrl-b n
\end{minted}

  \item You can tell that we’re in the first window because the asterisk is next to “0:bash”. Using Ctrl-b n again, brings us to window 2. To switch to the previous window, it’s Ctrl-b p. Now we’re back to controlling the first window.

  \begin{minted}[bgcolor=minterm,fontsize=\normalsize]{bash}
  Ctrl-b n
  \end{minted}

  \item If you want to jump to a specific window, you can use its index. So to go to what I’ve been calling window 3, use Ctrl-b 2, because 2 is the index of that window. The windows are zero-indexed, so the first window is index 0, the second window is index 1, the third window is index 2, and so on.

  \begin{minted}[bgcolor=minterm,fontsize=\normalsize]{bash}
  Ctrl-b 2
  \end{minted}

  \item You’ll notice that all the windows are named “bash”. By default, tmux names a new window based on the shell that is launched. It also dynamically changes the window’s name based on the program that is currently running. For example, if I run htop, the window name will change to “htop”. When “htop” is stopped, after a few seconds, tmux will detect I’m back at the bash prompt and update the window name accordingly. If you want to set an exact name for a window, you can do so with Ctrl-b ,. You’ll be prompted to enter a name.

  \begin{minted}[bgcolor=minterm,fontsize=\normalsize]{bash}
  Ctrl-b ,
  \end{minted}

  \item Again, when you’re done with a window or terminal, exit out of it like you would any other terminal by typing exit. Now the status line is updated and it shows only the remaining windows.

  \item I’m going to generate some ongoing text to demonstrate that the processes continue to run in the background while you are disconnected from tmux. This bit of code simply prints the number of seconds since January 1st, 1970.

\begin{minted}[bgcolor=minterm,fontsize=\normalsize]{bash}
while true
do
  date +"%s"
  sleep 1
done
\end{minted}

\end{itemize}
\sltitle{Detaching from a tmux session}

To detach from a session, type Ctrl-b d.

Ctrl-b d

Attaching to a tmux session

To attach or reattach to the most recently used tmux session, use tmux a. “a” is short for “attach-session”. Some people also use “attach”, so “tmux a”, “tmux attach”, and “tmux attach-session” are all the same command.

tmux a

Now we’re back where we left off, and as you can see, our program is still running in the background. Now that we’re connected to our session, we can resume working. For example, let’s say we want to switch to the other window, and we can do that by typing Ctrl-b n. That moves us to the next window. We can get back to the previous window again with Ctrl-b p. Again, our program is still running in the background just fine. If we need or want to disconnect again, leaving everything running, type Ctrl-b d.

Let’s start another session, and let’s call this one “coding” because let’s pretend we’re going to compile a program that can take a long time to compile.

tmux new -s coding

Now we’re in a new tmux session named “coding” with one window. Let’s detach from this session and let it run in the background by typing Ctrl-b d.
Listing tmux sessions

Now we have multiple sessions that are running in the background that we can attach to. Let’s say we created these sessions on a Friday, and we’ve had a long weekend, and we just can’t remember what we named them. Well, the good news is that you can list the available sessions with tmux ls. “ls” is a shortcut for list-sessions. I think “ls” is easier to remember, but some people prefer to use the long name of “list-sessions”, but use whatever works for you as they are the exact same command.

tmux ls

Now we see our two sessions, one named “backups” with two windows, and the other named “coding” with one window. Let’s say we want to attach to our “backup” session. To do that, we would use tmux a -t session\_name. If you just run tmux a, you’ll be connected to the most recently used Tmux session. If you want to specify the session, then use “-t” followed by the session name.

tmux a -t backups

Now we are attaching to the session named “backups”. As you can see, everything has continued to run while we’re away.

You can switch between sessions by using Ctrl-b s.

Ctrl-b s

This brings up a list of sessions. So instead of specifying a session name to attach to with tmux a -t session\_name, you could simply run tmux a, and if you are connected to a different
session than you want to be connected to, then you could use Ctrl-b s to choose the
correct session. Here, I’ll select the other session by hitting the down arrow and then pressing enter. Now we’re connected to that session and can resume our work here.
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
