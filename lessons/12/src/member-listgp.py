#fruits_list = ['apple', 'banana', 'orange', 'grape', 'pear']
def do():
    fruits_list = ['apple', 'banana', 'orange', 'grape', 'pear']
    'banana' in fruits_list
    'kiwi' in fruits_list

import timeit
t = timeit.Timer(setup='from __main__ import do', stmt='do()')
print(t.timeit())

import profile
profile.run('do()')
