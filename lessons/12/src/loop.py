def do():
    obj = ["hello", "my", "name", "is", "Delight", "!"]
    new = []
    for i in obj:
        new.append(i.upper())

import timeit
t = timeit.Timer(setup='from __main__ import do', stmt='do()')
print(t.timeit())
