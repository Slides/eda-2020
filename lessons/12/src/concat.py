import timeit
def do():
    obj = ["hello", "my", "name", "is", "Delight", "!"]
    s = ""
    for elem in obj:
        s += elem

t = timeit.Timer(setup='from __main__ import do', stmt='do()')
print(t.timeit())
