def toupper(x):
    return x.upper()

def do():
    obj = ["hello", "my", "name", "is", "Delight", "!"]
    map(toupper, obj)

import timeit
t = timeit.Timer(setup='from __main__ import do', stmt='do()')
print(t.timeit())
