import sys, os
import sqlite3


dbconn = sqlite3.connect('test.db')
cur = dbconn.cursor()
cur.execute("""
    CREATE TABLE IF NOT EXISTS example (
        id INTEGER,
        name TEXT,
        age INTEGER)
""")

vars = [
    [1, 'alice', 20],
    [2, 'bob', 30],
    [3, 'eve', 40],
    [4, 'lucy', 34]
]
for var in vars:

    cur.execute('SELECT * FROM example WHERE (id=? AND name=? AND age=?)', tuple(var))
    entry = cur.fetchone()
    if entry is None:
        cur.execute("INSERT INTO example VALUES (?,?,?)",tuple(var))
        print("==>",tuple(var),"New entry added")
    else:
        print("==>",tuple(var), 'Entry found')


#cur.execute("INSERT INTO example VALUES (1, 'alice', 20)")
#cur.execute("INSERT INTO example VALUES (2, 'bob', 30)")
#cur.execute("INSERT INTO example VALUES (3, 'eve', 40)")

dbconn.commit()
print("=======================")

cur.execute("SELECT * FROM example")
rows = cur.fetchall()

for row in rows:
    print("<==",row)

dbconn.close()
#https://www.ionos.it/digitalguide/siti-web/programmazione-del-sito-web/sqlite3-in-python/
