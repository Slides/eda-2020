import timeit

def do_1():
    list_object = []
    for i in range(100):
        list_object.append(i)

t = timeit.Timer(setup='from __main__ import do_1', stmt='do_1()')
print(t.timeit())
