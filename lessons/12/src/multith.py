# -*- coding: utf-8 -*-
import os, sys
import threading

import random


def calculate_squares(numbers):
    for num in numbers:
        square = num * num
        print(f"{num}|{square}|{threading.current_thread().name}|{os.getpid()}")
#            f"Square of the number {num} is {square} |
#Thread Name {threading.current_thread().name} |
#PID of the process {os.getpid()}"
#        )

import time

class procTimer():
    def __init__(self):
        self.tstart  = time.time()
        self.pstart = time.process_time()
        self.tstop = None
        self.pstop = None

    def start(self):
        self.tstart  = time.time()
        self.pstart = time.process_time()

    def stop(self):
        self.tstop  = time.time()
        self.pstop = time.process_time()

    def elapsed(self):
        self.telapsed = self.tstop - self.tstart
        self.pelapsed = self.pstop - self.pstart
        return [self.telapsed, self.pelapsed]

    def __str__(self):
        return f"{self.telapsed}, {self.pelapsed}"

import psutil

if __name__ == "__main__":
    maxNumber=1000
    numThreads = 4
    numbers = random.sample(range(0, maxNumber), maxNumber)
    print(f'number|square|thread|PID')

    size = len(numbers) // numThreads
    buff = []
    for a in range(numThreads):
        print(a,a*size,(a+1)*size)
        buff.append(numbers[a*size:(a+1)*size])

    # get the start time
    start_time = [time.time(), time.process_time()]
    pt = procTimer()

    th = []
    for a in range(numThreads):
        th.append(threading.Thread(target=calculate_squares, name=f"t{a}", args=(buff[a],)))
        th[a].start()

    for a in range(numThreads):
        th[a].join()

    # get the end time
    end_time = [time.time(), time.process_time()]
    pt.stop()
    elapsed = [end_time[k] - start_time[k] for k in range(2)]
    print(f"# elapsed {end_time[0]-start_time[0]}, {end_time[1]-start_time[1]}")
    print(f"# elapsed ",','.join(map(str, elapsed)))

    cela = pt.elapsed()
    print(f"# class elapsed {cela[0]}, {cela[1]}")
    print(pt)

    print(psutil.cpu_times())
    print(psutil.cpu_times(True))

    print(psutil.cpu_count())
    print(os.cpu_count())
    print(psutil.Process().cpu_affinity())
