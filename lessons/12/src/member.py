def do():
    fruits_list = ['apple', 'banana', 'orange', 'grape', 'pear']
    'banana' in fruits_list
    'kiwi' in fruits_list

import timeit
t = timeit.Timer(setup='from __main__ import do', stmt='do()')
t.timeit()
0.48580530006438494

>>> def do():
...     fruits_list = {'apple', 'banana', 'orange', 'grape', 'pear'}
...     'banana' in fruits_list
...     'kiwi' in fruits_list

>>> import timeit
>>> t = timeit.Timer(setup='from __main__ import do', stmt='do()')
>>> t.timeit()
