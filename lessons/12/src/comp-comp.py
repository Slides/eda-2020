import timeit

def do():
    [i for i in range(100)]

t = timeit.Timer(setup='from __main__ import do', stmt='do()')
print(t.timeit())
