def do():
    fruits_set = {'apple', 'banana', 'orange', 'grape', 'pear'}
    'banana' in fruits_set
    'kiwi' in fruits_set

import timeit
t = timeit.Timer(setup='from __main__ import do', stmt='do()')
print(t.timeit())
