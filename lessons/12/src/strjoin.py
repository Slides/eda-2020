import timeit

def do():
    s = ["hello", "my", "name", "is", "Delight", "!"]
    "".join(s)

t = timeit.Timer(setup='from __main__ import do', stmt='do()')
print(t.timeit())
