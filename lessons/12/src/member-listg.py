fruits_list = ['apple', 'banana', 'orange', 'grape', 'pear']
def do():
    'banana' in fruits_list
    'kiwi' in fruits_list

import timeit
t = timeit.Timer(setup='from __main__ import do', stmt='do()')
print(t.timeit())
