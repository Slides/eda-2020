from scipy.stats import norm
import matplotlib.pyplot as plt
import numpy as np

# calculate momens
mean, var, skew, kurt = norm.stats(moments='mvsk')

x=np.linspace(norm.ppf(0.01),norm.ppf(0.99), 100)

fig, ax = plt.subplots(1, 1)
ax.vlines(x, 0, norm.pdf(x), colors='b', lw=5, alpha=0.5)
#plt.show()
plt.savefig('normal.pdf')
