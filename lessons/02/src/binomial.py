from scipy.stats import binom
import matplotlib.pyplot as plt
import numpy as np

X=1/6;N=20

mean, var, skew, kurt = binom.stats(N, X, moments='mvsk')

x=np.arange(0,N+1)

fig, ax = plt.subplots(1, 1)
ax.vlines(x, 0, binom.pmf(x, N, X), colors='b', lw=5, alpha=0.5)
#plt.show()
plt.savefig('binomial.pdf')
