%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[allowframebreaks]{\insertsection: \insertsubsection}
%% The Example and Preliminary Observations
%%%%
\Large
\begin{itemize} \itemsep1.5em

\item
The idea now is that as $\theta$ varies through $[0,1]$ we have a distribution
$P(a,b|\theta)$.

What we want to do is multiply this by the constant that makes it integrate to $1$
so we can think of it as a probability distribution (normalization).

The normalization constant is just
\begin{equation*}
  B(a,b) = \int_0^1 \theta^a (1-\theta)^b d\theta
\end{equation*}

\item
It is called the \textbf{beta distribution}\\
(caution: the usual form is shifted from what I’m writing),\\
so we’ll just
write $\beta(a,b)$ for this.
%%\framebreak
% \item
% The number we multiply by is the inverse of
% \begin{equation*}
%   B(a,b) = \int_0^1 \theta^a (1-\theta)^b d\theta
% \end{equation*}
% called the (shifted) beta function.
%
% Again, just ignore that if it didn’t make sense.
% It’s just converting a distribution to a probability distribution.
%%I just know someone would call me on it if I didn’t mention that.

\item
This might seem unnecessarily complicated to start thinking of this as a
probability distribution in $\theta$, but it’s actually exactly what
we’re looking for.
\parbox{1\textwidth}{\centering
Consider the following three examples:
\vskip1em
\includegraphics[width=0.9\linewidth,keepaspectratio]{bs_example.png}
}
\framebreak
\item
The red one says the if we observe 2 heads and 8 tails, then the probability that
the coin has a bias towards tails is greater.
The mean happens at 0.20, but because we don’t have a lot of data, there is still
a pretty high probability of the true bias lying elsewhere.
\vskip1em
\includegraphics[width=0.9\linewidth,keepaspectratio]{bs_example.png}


\item
The middle one says that if we observe 5 heads and 5 tails, then the most probable
thing is that the bias is 0.5, but again there is still a lot of room for error.
\vskip1em
\includegraphics[width=0.9\linewidth,keepaspectratio]{bs_example.png}
\framebreak
\item
If we do a ton of trials to get enough data to be more confident in our guess,
then we see something like:
%\parbox{1\textwidth}{\centering
\includegraphics[width=.75\linewidth,keepaspectratio]{bs_example_2.png}\\
%}
Already at observing 50 heads and 50 tails we can say with 95\%
confidence that the true bias lies between 0.40 and 0.60.

\item
All right, you might be objecting at this point that this is just usual
statistics, where the heck is Bayes’ Theorem?

\centerline{You’d be right.}
Bayes’ Theorem comes in because we aren’t building our statistical model in a
vacuum.
We have prior beliefs about what the bias is.

\item
Let’s just write down Bayes’ Theorem in this case.\\
We want to know the probability of the bias, $\theta$, being some number given
our observations in our data.\\
We use the ''\emph{continuous form}`` of Bayes’ Theorem:
\begin{equation*}
  P(\theta|a,b) = \frac{P(a,b|\theta) P(\theta)}{\int_0^1 P(a,b|\theta) d\theta}
\end{equation*}

\item
I’m trying to give you a feel for Bayesian statistics, so I won’t work out in
detail the simplification of this.
Just note that the “posterior probability” (the left-hand side of the equation),
i.e. the distribution we get after taking into account our data, is the likelihood
times our prior beliefs divided by the evidence.

\item
Now, if you use that the denominator is just the definition of $B(a,b)$ and work
everything out it turns out to be another beta distribution!
It’s not a hard exercise if you’re comfortable with the definitions, but if
you’re willing to trust this, then you’ll see how beautiful it is to work
this way.

\item
If our prior belief is that the bias has distribution $\beta(x,y)$, then if our
data has $a$ heads and $b$ tails, we get
\begin{equation*}
  P(\theta|a,b) = \beta(a+x, b+y)
\end{equation*}
The way we update our beliefs based on evidence in this model is incredibly simple!

\item
Now I want to sanity check that this makes sense again.\\
Suppose we have absolutely no idea what the bias is.\\
It would be reasonable to make our prior belief $\beta(0,0)$, the flat line.\\
In other words, we believe ahead of time that all biases are equally likely.

\item
Now we run an experiment and flip 4 times.
We observe 3 heads and 1 tails.
Bayesian analysis tells us that our new (posterior probability) distribution is
$\beta(3,1)$:
\includegraphics[width=.9\linewidth,keepaspectratio]{bs_example_3.png}
Yikes! We don’t have a lot of certainty, but it looks like the bias is
heavily towards heads.

\item
\textbf{Danger}: This is because we used a terrible prior.
In the real world, it isn’t reasonable to think that a bias of 0.99 is just as
likely as 0.45.

\item
Let’s see what happens if we use just an ever so slightly more modest prior.
 We’ll use $\beta(2,2)$.
 This assumes the bias is most likely close to 0.5, but it is still very open
 to whatever the data suggests.

\item
In this case, our 3 heads and 1 tails tells us our updated belief is
$\beta(5,3)$:
\includegraphics[width=.9\linewidth,keepaspectratio]{bs_example_4.png}

\item
Ah. Much better.
We see a slight bias coming from the fact that we observed 3 heads and 1 tails.
This data can’t totally be ignored, but our prior belief tames how much we let
this sway our new beliefs.

\item
This is what makes Bayesian statistics so great!
If we have tons of prior evidence of a hypothesis, then observing a few outliers
shouldn’t make us change our minds.

\item
On the other hand, the setup allows us to change our minds, even if we are 99\%
certain about something -- as long as sufficient evidence is given.
This is just a mathematical formalization of the mantra:
\textbf{extraordinary claims require extraordinary evidence}.

\item
Not only would a ton of evidence be able to persuade us that the coin bias is 0.90,
but we should need a ton of evidence.
This is part of the shortcomings of non-Bayesian analysis.
It would be much easier to become convinced of such a bias if we didn’t have a
lot of data and we accidentally sampled some outliers.

\item
Now you should have an idea of how Bayesian statistics works.
In fact, if you understood this example, then most of the rest is just adding
parameters and using other distributions, so you actually have a really good
idea of what is meant by that term now.

\end{itemize}
\end{frame}
