%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{\insertsection: \insertsubsection}
%%[allowframebreaks]
%% The Example and Preliminary Observations
%%%%
\Large
\begin{itemize} \itemsep1.5em
\item
The main thing left to explain is what to do with all of this.
How do we draw conclusions after running this analysis on our data?

\item
You’ve probably often heard people who do statistics talk about “95\% confidence.”
Confidence intervals are used in every Statistics 101 class.
We’ll need to figure out the corresponding concept for Bayesian statistics.

\item
The standard phrase is something called the
\emph{highest density interval} (HDI).
The 95\% HDI just means that it is an interval for which the area under the
distribution is 0.95 (i.e. an interval spanning 95\% of the distribution)
such that every point in the interval has a higher probability than any point
outside of the interval:
\end{itemize}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{frame}
%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{\insertsection: \insertsubsection}
%[allowframebreaks]
%%%%%%%%%%%%%%%%%
\includegraphics[width=.8\linewidth,keepaspectratio]{bs_example_5.png}\\
(It doesn’t look like it, but that is supposed to be perfectly symmetrical.)
%\end{itemize}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{frame}
%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{\insertsection: \insertsubsection}
%[allowframebreaks]
%%%%%%%%%%%%%%%%%
\includegraphics[width=.8\linewidth,keepaspectratio]{bs_example_6.png}
\end{frame}
%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{\insertsection: \insertsubsection}
%[allowframebreaks]
%%%%%%%%%%%%%%%%%
\Large
\begin{itemize} \itemsep1.5em

\item The first is the correct way to make the interval. Notice all points on the
curve over the shaded region are higher up (i.e. more probable) than points on
the curve not in the region.

\item
\textbf{Note}: There are lots of 95\% intervals that are not HDI’s.
The second picture is an example of such a thing because even though the area
under the curve is 0.95, the big purple point is not in the interval but is
higher up than some of the points off to the left which are included in the
interval.

\item
Lastly, we will say that a hypothesized bias $\theta_0$ is credible if some
small neighborhood of that value lies completely inside our 95\% HDI.
That small threshold is sometimes called the region of practical equivalence
(ROPE) and is just a value we must set.


\end{itemize}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{frame}
%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{\insertsection: \insertsubsection}
%[allowframebreaks]
%%%%%%%%%%%%%%%%%
\Large
\begin{itemize} \itemsep1.5em
  \item
  If we set it to be 0.02, then we would say that the coin being fair is a credible
  hypothesis if the whole interval from 0.48 to 0.52 is inside the 95\% HDI.

  \item
  A note ahead of time, calculating the HDI for the beta distribution is actually
  kind of a mess because of the nature of the function.
  There is no closed-form solution, so usually, you can just look these things
  up in a table or approximate it somehow.

\item
Both the mean
\begin{equation*}
    \mu = a/(a+b)
\end{equation*}
and the standard deviation
\begin{equation*}
    \sigma = \left( \frac{\mu(1-\mu)}{a+b+1} \right)^{1/2}
\end{equation*}
do have closed forms.
\end{itemize}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{frame}
%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{\insertsection: \insertsubsection}
%[allowframebreaks]
%%%%%%%%%%%%%%%%%
\Large
\begin{itemize} \itemsep1.5em
\item
Thus I’m going to approximate for the sake of this article using the
\emph{''two standard deviations``} rule that says that two standard deviations
on either side of the mean is roughly 95\%.

\item
Caution, if the distribution is highly skewed, for example,
$\beta(3,25)$ or something, then this approximation will actually be way off.

\item
Let’s go back to the same examples from before and add in this new terminology
to see how it works.
Suppose we have absolutely no idea what the bias is and we make our prior
belief $\beta(0,0)$, the flat line.

This says that we believe ahead of time that all biases are equally likely.
Now we do an experiment and observe 3 heads and 1 tails.

Bayesian analysis tells us that our new distribution is $\beta(3,1)$.

\end{itemize}
%%%%
\end{frame}
%%%%
\begin{frame}{\insertsection: \insertsubsection}
%%%%%%%%%%%%%%%%%
\Large
\begin{itemize} \itemsep1.5em

\item
The 95\% HDI in this case is approximately 0.49 to 0.84.
Thus we can say with 95\% certainty that the true bias is in this region.
Note that it is not a credible hypothesis to guess that the coin is fair
(bias of 0.5) because the interval [0.48, 0.52] is not completely within the HDI.

\item
This example really illustrates how choosing different thresholds can matter,
because if we picked an interval of 0.01 rather than 0.02, then the hypothesis
that the coin is fair would be credible
(because [0.49, 0.51] is completely within the HDI).

\item
Let’s see what happens if we use just an ever so slightly more reasonable prior.
We’ll use $\beta(2,2)$.
This gives us a starting assumption that the coin is probably fair, but it
is still very open to whatever the data suggests.
\end{itemize}
%%%%
\end{frame}
%%%%
\begin{frame}{\insertsection: \insertsubsection}
%%%%%%%%%%%%%%%%%
\Large
\begin{itemize} \itemsep1.5em
\item
In this case, our 3 heads and 1 tails tells us our posterior distribution is
$\beta(5,3)$.
The 95\% HDI is 0.45 to 0.75.
Using the same data we get a little bit more narrow of an interval here,
but more importantly, we feel much more comfortable with the claim that the
coin is fair.

It is a credible hypothesis.

\item
This brings up a sort of \emph{''statistical uncertainty principle``}.
If we want a ton of certainty, then it forces our interval to get wider and
wider.
This makes intuitive sense, because if I want to give you a range that I’m
99.9999999\% certain the true bias is in, then I better give you practically
every possibility.
\end{itemize}
%%%%
\end{frame}
%%%%
\begin{frame}{\insertsection: \insertsubsection}
%%%%%%%%%%%%%%%%%
\Large
\begin{itemize} \itemsep1.5em
\item
If I want to pinpoint a precise spot for the bias, then I have to give up
certainty (unless you’re in an extreme situation where the distribution is a really sharp spike).
You’ll end up with something like: I can say with 1\% certainty that the true
bias is between 0.59999999 and 0.6000000001.

\item
We’ve locked onto a small range, but we’ve given up certainty.
Note the similarity to the Heisenberg uncertainty principle which says the more
precisely you know the momentum or position of a particle the less precisely
you know the other.

\end{itemize}

\end{frame}
