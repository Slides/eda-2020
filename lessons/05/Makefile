SHELL=/bin/bash
DOC     := $(shell basename eda-??.tex .tex)
#BIBLIO  := $(DOC).bib
LATEX   := pdf
FLAGS   := -quiet -bibtex -shell-escape

KEYWORD := Experimental data analysys, statistics, modelling
VPATH   := ../common:src

#%.pdf: %.ipynb
#	jupyter nbconvert $< --to latex
#	pdflatex $(shell basename $<).tex

all:
	@echo "processing $$DOC"
	latexmk -$(LATEX) $(FLAGS) $(DOC).tex

verbose :
	latexmk -g -$(LATEX) $(FLAGS) -verbose $(DOC).tex

check:
	pdflatex $(DOC)

clean :
	latexmk -c
#	rm -f *.lol *.aux *.bbl *.aux *.log *.nav *.out *.snm *.toc *.vrb *.auxlock *.fdb_latexmk *.fls
	rm -rf *.nav *.snm slides/*.aux *.vrb *.fdb_latexmk *.fls

cleanall : clean
	latexmk -C -bibtex
	rm -rf $(DOC).pdf $(DOC)-final.pdf

pdf : all
	# GPL Ghostscript 9.05: Set UseCIEColor for UseDeviceIndependentColor to work properly.
	pdftk $(DOC).pdf dump_data output $(DOC).meta
	#			-dUseCIEColor \ version < 9.11
	ghostscript \
			-sDEVICE=pdfwrite -dCompatibilityLevel=1.4\
			-dPDFSETTINGS=/printer \
			-dEmbedAllFonts=true -dSubsetFonts=true \
			-dNOPAUSE -dQUIET -dBATCH \
			-sOutputFile=$(DOC)-gs.pdf $(DOC).pdf
	@echo "InfoKey: Keywords" >> $(DOC).meta
	@echo "InfoValue: $(KEYWORDS)" >> $(DOC).meta
	#
	pdftk $(DOC)-gs.pdf update_info $(DOC).meta output $(DOC)-final.pdf
	rm -rf $(DOC)-gs.pdf $(DOC).meta
	@ls -lh $(DOC)*.pdf
#

help :
	@echo ""
	@echo "creates pdf presentation using latexmk"
	@echo " make          : generate presentation pdf"
	@echo " make pdf      : generate optimized pdf"
	@echo " make verbose  : show latex compiler output"
	@echo " make clean    : delete temporary files"
	@echo " make cleanall : delete all temporary files and output"
	@echo ""
