import matplotlib.pyplot as plt
plt.style.use('classic')
##%matplotlib inline

import numpy as np
import pandas as pd
import scipy.stats as stats

import seaborn as sns
sns.set()

# Load dataset
mtcars = pd.read_csv('src/corrtest/mtcars.csv')
print(mtcars)

jp = sns.jointplot(data=mtcars,
    x='mpg',
    y='wt',
    kind='reg', color='b')
#plt.savefig("/home/eda/eda-2020/lessons/08/figures/corrtest-raw.pdf")
jp.annotate(stats.pearsonr)
# control x and y limits
plt.ylim(0, 6)
plt.xlim(10, 35)
#plt.show()
plt.savefig("/home/eda/eda-2020/lessons/08/figures/corrtest.pdf")

#jp.annotate(template=""{stat}: {val:.2f}"", stat="pearsonr")
#plt.savefig("/home/eda/eda-2020/lessons/08/figures/corrtest-ann.pdf")

print(mtcars[['mpg','wt']].corr())
r, pvalue = stats.pearsonr(mtcars['mpg'],mtcars['wt'])
print("r=",r,"pvalue=",pvalue)

#Calculates the T-test on TWO RELATED samples of scores, a and b.
print(stats.ttest_rel(mtcars['mpg'].to_numpy()
    ,mtcars['wt'].to_numpy())) #, equal_var = False))
#If the p-value is smaller than the threshold, e.g. 1%, 5% or 10%,
#then we reject the null hypothesis of equal averages.
print(mtcars['mpg'].describe())
print(mtcars['wt'].describe())

print(stats.linregress(mtcars['mpg'].to_numpy()
    ,mtcars['wt'].to_numpy())
    )
