#echo 10 | awk 'BEGIN{print "x,y";}{for(x=0; x<$1; x+=0.1) print x","x+sin(x);}' > src/corr-nonlin/nonlin.csv

import matplotlib.pyplot as plt
plt.style.use('classic')
##%matplotlib inline

import numpy as np
import pandas as pd
import scipy.stats as stats

import seaborn as sns
sns.set()

# Load dataset
xy = pd.read_csv('src/corr-nonlin/nonlin.csv')
print(xy)
print(xy.describe())

jp = sns.jointplot(data=xy,
    x='x',
    y='y',
    kind='reg', color='r')
jp.annotate(stats.pearsonr)
# control x and y limits
plt.ylim(-0.2, 10.2)
plt.xlim(-0.2, 10.2)
#plt.show()
plt.savefig("/home/eda/eda-2020/lessons/08/figures/corr-nonlin.pdf")

print(xy.corr())
r, pvalue = stats.pearsonr(xy['x'],xy['y'])
print("r=",r,"pvalue=",pvalue)

#Calculates the T-test on TWO RELATED samples of scores, a and b.
print(stats.ttest_rel(xy['x'].to_numpy()
    ,xy['y'].to_numpy())) #, equal_var = False))
#If the p-value is smaller than the threshold, e.g. 1%, 5% or 10%,
#then we reject the null hypothesis of equal averages.

print(stats.linregress(
    xy['x'].to_numpy(),
    xy['y'].to_numpy())
    )
