import sys
import matplotlib.pyplot as plt
#
import numpy as np
import pandas as pd
#
import seaborn as sns
import timeit

# Load dataset
titanic = sns.load_dataset('titanic')
print(titanic)

print(titanic.info())

# check correlations
tc=titanic.corr()
print(tc)
hm = sns.heatmap(tc, annot=True, fmt=".2f")
#plt.show()
plt.savefig("../../figures/titanic-heatmap.pdf")

#like in R
print(titanic.describe())

# subsetting
subti = titanic[
    (titanic.sex == 'female')
    & (titanic['class'].isin(['First', 'Third']))
    & (titanic.age > 30)
    & (titanic.survived == 0)
]
print(subti)

# non categorical variables, just drop non numeric
ntitanic = titanic.select_dtypes([np.number])
print(ntitanic.describe())
#sys.exit()

# pairplot with linear model
pp = sns.pairplot(ntitanic, kind="reg")
#plt.show()
plt.savefig("../../figures/titanic-pairplot-lin.pdf")

print(pp)
