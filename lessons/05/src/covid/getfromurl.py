import pandas as pd

import requests
from io import StringIO
raw_url="https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-andamento-nazionale/dpc-covid19-ita-andamento-nazionale.csv"
url="https://github.com/pcm-dpc/COVID-19/blob/master/dati-andamento-nazionale/dpc-covid19-ita-andamento-nazionale.csv"

#headers = {"User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:66.0) Gecko/20100101 Firefox/66.0"}
#req = requests.get(url, headers=headers)
#print(req.text)

covid=pd.read_csv(raw_url, error_bad_lines=False)

# s=requests.get(url).content
# covid=pd.read_csv(io.StringIO(s.decode('utf-8')))
#covid=pd.read_csv(StringIO(req.text))


print(covid)
print(covid.info())
