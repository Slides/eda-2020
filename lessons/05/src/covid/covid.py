import sys
import matplotlib.pyplot as plt
plt.tight_layout()
plt.style.use('classic')
#
import numpy as np
import pandas as pd
#
import seaborn as sns
sns.set()

import timeit

# Load dataset
covid = pd.read_csv('/home/eda/eda-2020/lessons/08/src/covid/dpc-covid19-ita-andamento-nazionale.csv')
print(covid.describe())

num_covid = covid.select_dtypes([np.number])

cc = covid.corr()
print(cc)

hm = sns.heatmap(num_covid, annot=True, fmt=".2f")
#hm = sns.heatmap(num_covid)

#plt.show()
plt.savefig("/home/eda/eda-2020/lessons/08/figures/covid-heatmap.pdf")

print(covid.info())

print(num_covid.info())

print(num_covid)

# pairplot with linear model
#pp = sns.pairplot(num_covid, kind="reg")
#plt.show()
#plt.savefig("../../figures/covid-pairplot-lin.pdf")

#variazione_totale_positivi
#nuovi_positivi

import scipy.stats as stats
jp = sns.jointplot(data=num_covid, x='nuovi_positivi', y='variazione_totale_positivi', kind='reg', color='g')
jp.annotate(stats.pearsonr)
#plt.show()
plt.savefig("/home/eda/eda-2020/lessons/08/figures/covid-jointplot.pdf")

print(num_covid[['nuovi_positivi', 'variazione_totale_positivi']])
print(num_covid[['nuovi_positivi', 'variazione_totale_positivi']].corr())
print(num_covid['nuovi_positivi'])
print(num_covid['nuovi_positivi'].to_numpy())

pear = stats.pearsonr(
    num_covid['nuovi_positivi'].to_numpy(),
    num_covid['variazione_totale_positivi'].to_numpy()
    )
print(pear)

#jp = jp.annotate(stats.pearsonr, fontsize=18, loc=(0.1, 0.8)).
##You will have to do from scipy import stats.
#scipy.stats.linregress(x, y)

print(num_covid.corr())
hm = sns.heatmap(num_covid, annot=True, fmt=".2f")
#hm = sns.heatmap(num_covid)

#plt.show()
plt.savefig("/home/eda/eda-2020/lessons/08/figures/covid-heatmap.pdf")


x='nuovi_positivi'
X=num_covid[x]

y='variazione_totale_positivi'
#y='totale_positivi'
Y=num_covid[y]

print(X.describe())
print("NaN = ",X.isna().sum())

print(Y.describe())
print("NaN = ",Y.isna().sum())

pear = stats.pearsonr(X.dropna().to_numpy(),Y.dropna().to_numpy())
print(pear)

jp = sns.jointplot(data=num_covid,
    x=x,
    y=y, kind='reg', color='g')
jp.annotate(stats.pearsonr)
#plt.show()
plt.savefig("/home/eda/eda-2020/lessons/08/figures/covid-jointplot-dN.pdf")
print(num_covid[[x,y]].corr())

import scipy.stats
print(scipy.stats.pearsonr(X, Y))

# pairplot with linear model
pp = sns.pairplot(num_covid, kind="reg")
#plt.show()
plt.savefig("/home/eda/eda-2020/lessons/08/figures/covid-pairplot-lin.pdf")
