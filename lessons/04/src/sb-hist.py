# firts comment
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
# second comment
plt.style.use('classic')
import seaborn as sns
sns.set()

data = np.random.multivariate_normal([0, 0], [[5, 2], [2, 2]], size=2000)
data = pd.DataFrame(data, columns=['x', 'y'])

for col in 'xy':
    plt.hist(data[col], density=True, alpha=0.5)
plt.savefig('figures/sb-hist.pdf')

for col in 'xy':
    sns.kdeplot(data[col], shade=True)
plt.savefig('figures/sb-kdeplot.pdf')

sns.distplot(data['x'])
sns.distplot(data['y']);
plt.savefig('figures/sb-distplot.pdf')

sns.kdeplot(data);
plt.savefig('figures/sb-kde-2D.pdf')

with sns.axes_style('white'):
    sns.jointplot("x", "y", data, kind='kde');
plt.savefig('figures/sb-joinplot.pdf')

with sns.axes_style('white'):
    sns.jointplot("x", "y", data, kind='hex')
plt.savefig('figures/sb-joinplot-hex.pdf')

tips = sns.load_dataset('tips')
# print(tips.head())
#    total_bill   tip     sex smoker  day    time  size
# 0       16.99  1.01  Female     No  Sun  Dinner     2
# 1       10.34  1.66    Male     No  Sun  Dinner     3
# 2       21.01  3.50    Male     No  Sun  Dinner     3
# 3       23.68  3.31    Male     No  Sun  Dinner     2
# 4       24.59  3.61  Female     No  Sun  Dinner     4

tips['tip_pct'] = 100 * tips['tip'] / tips['total_bill']

grid = sns.FacetGrid(tips, row="sex", col="time", margin_titles=True)
grid.map(plt.hist, "tip_pct", bins=np.linspace(0, 40, 15));
plt.savefig('figures/sb-hist-grid.pdf')

with sns.axes_style(style='ticks'):
    g = sns.factorplot("day", "total_bill", "sex", data=tips, kind="box")
    g.set_axis_labels("Day", "Total Bill");
plt.savefig('figures/sb-factor-plot.pdf')



sns.jointplot("total_bill", "tip", data=tips, kind='reg');
plt.savefig('figures/sb-joint-distribution.pdf')

# !curl -O https://raw.githubusercontent.com/jakevdp/marathon-data/master/marathon-data.csv
data = pd.read_csv('src/marathon-data.csv')
#data.head()
from pandas import datetools
import datetime
from datetime import timedelta

def convert_time(s):
    h, m, s = map(int, s.split(':'))
    return pd.datetools.timedelta(hours=h, minutes=m, seconds=s)

data = pd.read_csv('src/marathon-data.csv',
                   converters={'split':convert_time, 'final':convert_time})
#data.head()

data['split_sec'] = data['split'].astype(int) / 1E9
data['final_sec'] = data['final'].astype(int) / 1E9

with sns.axes_style('white'):
    g = sns.catplot("split_sec", "final_sec", data, strip='hex')
    g.ax_joint.plot(np.linspace(4000, 16000),
                    np.linspace(8000, 32000), ':k')
plt.savefig('figures/sb-marathon.pdf')
