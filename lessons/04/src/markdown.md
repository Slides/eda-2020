# Title
## Chapter
===
### Section
---
#### Subsection
normal text paragraph
* items
* items

[I'm an inline-style link](https://www.google.com), tihs is **bold**, _italic_, ~~false~~

This is highlighted code
```python
from scipy.stats import binom
import matplotlib.pyplot as plt
import numpy as np

X=1/6;N=20

mean, var, skew, kurt = binom.stats(N, X, moments='mvsk')
```
