#import matplotlib
#matplotlib.use('Agg')

import matplotlib.pyplot as plt
plt.style.use('classic')
##%matplotlib inline

import numpy as np
import pandas as pd

# Create some data
rng = np.random.RandomState(0)
x = np.linspace(0, 10, 500)
y = np.cumsum(rng.randn(500, 6), 0)

# Plot the data with Matplotlib defaults
plt.plot(x, y)
plt.legend('ABCDEF', ncol=2, loc='upper left');
plt.savefig('figures/matplotlib.pdf')

import seaborn as sns
sns.set()

# Plot the data with Matplotlib defaults
plt.plot(x, y)
plt.legend('ABCDEF', ncol=2, loc='upper left');
plt.savefig('figures/seaborn.pdf')

df = sns.load_dataset('iris')
sns_plot = sns.pairplot(df, hue='species', height=2.5)
sns_plot.savefig("figures/sb-pairplot.pdf")

data = np.random.multivariate_normal([0, 0], [[5, 2], [2, 2]], size=2000)
data = pd.DataFrame(data, columns=['x', 'y'])

for col in 'xy':
    plt.hist(data[col], density=True, alpha=0.5)
plt.savefig('figures/sb-hist.pdf')
