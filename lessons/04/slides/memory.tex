%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{\insertsection: \insertsubsection}
%%
%% https://stackify.com/python-garbage-collection/
%%
%%%%\item
\Large
\begin{itemize}
  \item A programming language uses objects in its programs to perform operations. Objects include simple variables, like strings, integers, or booleans.

  They also include more complex data structures like lists, hashes, or classes.
  \item The values of your program’s objects are stored in memory for quick access. In many programming languages, a variable in your program code is simply a pointer to the address of the object in memory. When a variable is used in a program, the process will read the value from memory and operate on it.
  \item In early programming languages, developers were responsible for all memory management in their programs. This meant before creating a list or an object, you first needed to allocate the memory for your variable. After you were done with your variable, you then needed to deallocate it to “free” that memory for other users.
\end{itemize}
\end{frame}

\begin{frame}{\insertsection: \insertsubsection}
  \Large
  This led to two problems:
  \vskip 2em
  \begin{enumerate}
    \item \textbf{Forgetting to free your memory}. If you don’t free your memory when you’re done using it, it can result in memory leaks. This can lead to your program using too much memory over time. For long-running applications, this can cause serious problems.
    \item \textbf{Freeing your memory too soon}. The second type of problem consists of freeing your memory while it’s still in use. This can cause your program to crash if it tries to access a value in memory that doesn’t exist, or it can corrupt your data. A variable that refers to memory that has been freed is called a dangling pointer.
  \end{enumerate}
\vskip 2em
These problems were undesirable, and so newer languages added automatic memory management.
\end{frame}

\subsubsection{Automatic memory management and garbage collection}
\begin{frame}{\insertsection: \insertsubsection}
  \sltitle{Automatic memory management and garbage collection}
  \Large
  \begin{itemize}
    \item With automatic memory management, programmers no longer needed to manage memory themselves. Rather, the runtime handled this for them.
    \item There are a few different methods for automatic memory management, but one of the more popular ones uses \textbf{reference counting}.
    \item With reference counting, the runtime keeps track of all of the references to an object. When an object has zero references to it, it’s unusable by the program code and thus able to be deleted.
  \end{itemize}

For programmers, automatic memory management adds a number of benefits. It’s \HiLi{faster to develop programs} without thinking about low-level memory details. Further, it can help \HiLi{avoid costly memory leaks} or dangerous dangling pointers.
\end{frame}

\begin{frame}{\insertsection: \insertsubsection}
  \Large
However, \HiLi{automatic memory management comes at a cost}.
\vskip 2em
\begin{itemize}
  \item Your program will need to use additional memory and computation to track all of
   its references.
   \item What’s more, many programming languages with automatic memory management use a “stop-the-world” process for garbage collection where all execution stops while the garbage collector looks for and deletes objects to be collected.

\end{itemize}

\vskip 2em
Today, most modern programming languages like \HiLi{Java}, \HiLi{Python}, and \HiLi{Golang} use automatic memory management.
\end{frame}

\begin{frame}{\insertsection: \insertsubsection}
  \Large
For long-running applications where performance is critical, some languages still have manual memory management.
\vskip 2em
\begin{itemize}
  \item The classic example of this is \HiLi{C/C++}.
  \item We also see manual memory management in \HiLi{Objective-C}, the language used for macOS and iOS.
  \item For newer languages, \HiLi{Rust} uses manual memory management.
\end{itemize}
\end{frame}

\subsubsection{How Python implements garbage collection}
\begin{frame}[fragile]{\insertsection: \insertsubsection}

\sltitle{How Python implements garbage collection}
\Large

\HiLi{CPython} is the most widely used implementation.
\vskip 2em
However, there are other implementations of Python, such as
\vskip 1em
\begin{itemize}
  \item \HiLi{PyPy},
  \item \HiLi{Jython} (Java-based), or
  \item \HiLi{IronPython} (C\#-based).
\end{itemize}
\vskip 2em
%To see which Python you’re using, run the following command in your terminal:
% \begin{verbatim}
% python -c 'import platform; \
%   print(platform.python_implementation())'
% \end{verbatim}
% \begin{lstlisting}[basicstyle=\large,backgroundcolor=\color{codegray}]
% $ python -c 'import platform; \
%     print(platform.python_implementation())'
% \end{lstlisting}

\end{frame}
\begin{frame}[fragile]{\insertsection: \insertsubsection}
  \Large
There are two aspects to memory management and garbage collection in CPython:
\vskip 1em
\begin{enumerate}
  \item Reference counting
  \item Generational garbage collection
\end{enumerate}
\vskip 1em
To see which Python you’re using, run the following command in your terminal:
% \begin{verbatim}
% python -c 'import platform; \
%   print(platform.python_implementation())'
% \end{verbatim}
\begin{lstlisting}[basicstyle=\large,backgroundcolor=\color{codegray}]
$ python -c 'import platform; \
    print(platform.python_implementation())'
\end{lstlisting}
\vskip 1em
\begin{itemize}
  \item The way Python manages memory make it hard for long running programs.
  \item When you explicitly free an object with del statement, CPython necessarily does not return allocated memory to the OS.
  \item It keeps the memory for further use in future.

\end{itemize}

\end{frame}

\subsubsection{Reference counting in CPython}
\begin{frame}{\insertsection: \insertsubsection}
\sltitle{Reference counting in CPython}
\Large

The main garbage collection mechanism in CPython is through \HiLi{reference counts}.
\vskip 1em
\begin{itemize}
  \item Whenever you create an object in Python, the underlying C object has both a Python type (such as list, dict, or function) and a reference count.
  \item At a very basic level, a Python object’s reference count is incremented whenever the object is referenced, and it’s decremented when an object is dereferenced. If an object’s reference count is 0, the memory for the object is deallocated.
  \item Your program’s code \HiLi{can’t disable Python’s reference counting}. This is in contrast to the generational garbage collector discussed below.
\end{itemize}
\vskip 1em
It does have some downsides, including an inability to detect \HiLi{cyclic references}.
%  as discussed below.
%
% However, reference counting is nice because you can immediately remove an object when it has no references.
\end{frame}

%% check
\subsubsection{Viewing reference counts in Python}
\begin{frame}[fragile]{\insertsection: \insertsubsection}
  \sltitle{Viewing reference counts in Python}
  \Large
  \begin{lstlisting}[basicstyle=\large,backgroundcolor=\color{codegray}]
  $ python
Python 3.7.4 (default, Nov 22 2019, 21:31:39)
...
>>> import sys
>>> a = 'a string'
>>> sys.getrefcount(a)
2
>>> sys.getsizeof(a)
57
\end{lstlisting}
\end{frame}

\subsubsection{Generational garbage collector}
\begin{frame}[fragile]{\insertsection: \insertsubsection}
  \Large
\sltitle{Generational garbage collector}

%There are two key concepts to understand with the generational garbage collector. The first concept is that of a generation.
\begin{itemize}
  \item \HiLi{generation}
  The garbage collector is keeping track of all objects in memory. A new object starts its life in the first generation of the garbage collector. If Python executes a garbage collection process on a generation and an object survives, it moves up into a second, older generation. The Python garbage collector has three generations in total, and an object moves into an older generation whenever it survives a garbage collection process on its current generation.
  \item \HiLi{threshold}
  For each generation, the garbage collector module has a threshold number of objects. If the number of objects exceeds that threshold, the garbage collector will trigger a collection process. For any objects that survive that process, they’re moved into an older generation.
\end{itemize}
The behavior of the generational garbage collector can be controlled using the \textbf{gc module}.
Yuo can change the thresholds, trigger a garbage collection process, or disable the garbage collection process.
\end{frame}

\subsubsection{Collecting garbage}
\begin{frame}[fragile]{\insertsection: \insertsubsection}
  \sltitle{Collecting garbage}
\begin{lstlisting}[basicstyle=\large,backgroundcolor=\color{codegray}]
$ python
Python 3.7.4 (default, Nov 22 2019, 21:31:39)
[GCC 7.3.0] :: Intel(R) Corporation on linux
Type "help", "copyright", "credits" or "license" for more information.
Intel(R) Distribution for Python is brought to you by Intel Corporation.
Please check out: https://software.intel.com/en-us/python-distibution
>>> import gc
>>> gc.get_count()
(309, 5, 1)
>>> gc.collect()
0
>>> gc.get_count()
(20, 0, 0)
\end{lstlisting}
\end{frame}

\subsubsection{Strategy}
\begin{frame}{\insertsection: \insertsubsection}
  \sltitle{Strategy}
  \large
  \vskip 1em
  Python garbage collector does a good job on the average
  \begin{itemize}
    \item tweacking garbage collector is for professionals (few reports of best performances)
    \item forcing \texttt{gc.collect()} can help, but generally slows down
    \item limit the contex of (big) variables using functions/methods as much as possible
    \item use optimized python distributions like the Intel one.
  \end{itemize}
  \vskip 2em
  To achieve better performance and lower memory profile
  it's better to invest time in structuring properly the code.
%%\end{frame}\begin{frame}{\insertsection: \insertsubsection}
%%\sltitle{What It Is and How It Works}
%  \begin{textblock*}{.48\textwidth}(\tblc,\tbuc)
% %\onslide<+->{
%   \begin{block}{TITLE LEFT}
%    block text
%    \begin{itemize} %%[leftmargin=*]
%     \item item
%    \end{itemize}
%   \end{block}
% %}
%  \end{textblock*}
% %%%%
% %%%%
% \begin{textblock*}{.48\textwidth}(\tbrc,\tbuc)
% %\onslide<+->{
% \begin{block}{TITLE RIGHT}
% block text
% \begin{itemize} %%[leftmargin=*]
% \item item
% \end{itemize}
% \end{block}
% %}
% \end{textblock*}
% %%%%
% %%%%
% \begin{textblock*}{.5\textwidth}(9em,9em)
% %\onslide<+->{
%  \begin{exampleblock}{}
%     \begin{tikzpicture}
%         \node[inner sep=0pt] (pic) at (0,0)
%             {\includegraphics[width=1\linewidth,keepaspectratio]{{figure.png}}};
%         \node[draw,align=left] at (-2,1.75) {figure *};
%     \end{tikzpicture}
%  \end{exampleblock}
% %}
% \end{textblock*}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{frame}
