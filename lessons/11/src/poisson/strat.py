# -*- coding: utf-8 -*-
import os, sys
import pandas as pd
import numpy as np

def getFrequencyTable(inFile, cIntervals=10, range=None):
    layers = pd.read_csv(inFile)
    cLayers = layers.shape[0]
    if range:
        layers = layers.append({'depth':range[0]}, ignore_index=True)
        layers = layers.append({'depth':range[-1]}, ignore_index=True)
    #print(layers)
    #sys.exit()

    counts = layers.iloc[:,0].value_counts(bins=cIntervals).sort_index()
    #print(counts)
    npcounts=counts.to_numpy()
    if range:
        npcounts[0]-=1
        npcounts[-1]-=1
    #print(npcounts)
    return cLayers, np.array(np.unique(npcounts, return_counts=True)).T

from scipy.stats import poisson
def getExpected(n,T,freq_tbl):
    Exp = np.array([T*poisson.pmf(f[0], n/T) for f  in freq_tbl], ndmin=2)
    return np.hstack((freq_tbl,Exp.T))

from scipy.stats import chi2
def getChiSquare(tbl, alpha=0.05):
    #print(tbl)
    chi2_tbl = np.hstack((tbl,np.array([(f[1]-f[2])**2/f[2] for f  in tbl], ndmin=2).T))
    print("# k          Obs        Exp        chi2")
    print(chi2_tbl)
    chi_val = chi2.ppf(1-alpha, chi2_tbl.shape[0]-1)
    chi_sum = chi2_tbl.sum(axis=0)
    #print(chi2.ppf(1-alpha,2) )
    #print(chi_sum[-1])
    return chi_sum[-1]<chi_val, chi_val, chi2_tbl.sum(axis=0)

T=15
range=[0,45]
range=None

n, freq_tbl=getFrequencyTable('data/strat.dat',T, range=range)
print('Number of events: n=',n)
print('Number of intervals: T=',T)
print('Range:',range)
print("Frequency table")
print(freq_tbl)

OE_tbl = getExpected(n,T,freq_tbl)

chi_pass,chiv,chis=getChiSquare(OE_tbl)
print('# test')
print('H0 :',chi_pass,'(%g < %g)' % (chis[-1],chiv))

#chi_pass,chiv,chis=getChiSquare(OE_tbl,alpha=0.01)
#print(chi_pass,chiv,chis)

#f_t=np.array(np.unique(npcounts, return_counts=True)).T
#print(f_t)
