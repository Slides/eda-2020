# -*- coding: utf-8 -*-
import os, sys
import numpy as np

len=10
samp=100

t = np.linspace(0,len,num=samp*len, endpoint=False)
print(t)
s = np.sin(2*np.pi*t)
print(s)
