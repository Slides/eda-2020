# eda-2020
## Experimental Data Analysis (Roma3 PHD course)
### by Aladino Govoni (<aladino.govoni@ingv.it>)
## Licensing
Shield: [![CC BY 4.0][cc-by-shield]][cc-by]

This work is licensed under a [Creative Commons Attribution 4.0 International
License][cc-by].

[![CC BY 4.0][cc-by-image]][cc-by]

[cc-by]: http://creativecommons.org/licenses/by/4.0/
[cc-by-image]: https://i.creativecommons.org/l/by/4.0/88x31.png
[cc-by-shield]: https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg


## Useful docs
* [git - the simple guide](https://rogerdudler.github.io/git-guide/)
* [Mastering Markdown](https://guides.github.com/features/mastering-markdown/)
* [Mastering tags](https://blog.daftcode.pl/how-to-become-a-master-of-git-tags-b70fbd9609d9)
* [Markdown quick reference](https://www.markdownguide.org/basic-syntax/)
