## Remove committed files from Git version control

  1. Create a ```.gitignore``` file, if you haven’t already
  2. Edit ```.gitignore``` to match the file/folder you want to ignore
  3. Execute the following command: ```git rm [-r] --cached path/to/file```.
Git will list the files it has deleted. The ```--cached``` flag should be used if you want to keep the local copy but remove it from the repository (use ```-r``` to delete recursively a path).

  4. Verify that these files are being deleted from version control using ```git status```.
  5. Push the changes to the repository

After completing these steps, those files should be removed from version control but not removed from any local machine. Any other developer which pulls again after you pushed your changes should have no issues.

Note: it’s still possible to retrieve such data from Git history. Therefore, you need to remove it from Git history if it’s sensitive data you want to get rid of. Check out [huffleHog](https://github.com/dxa4481/truffleHog) in case you want to remove such data from your Git repository.

From https://medium.com/better-programming/how-to-remove-committed-files-from-git-version-control-b6533b8f9044
