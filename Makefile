LESSONS := $(shell find lessons -mindepth 2 -type d -name slides -exec dirname {} \; | sort)

all clean cleanall pdf :
	for dir in $(LESSONS); do \
		make -C $$dir $@ ; \
	done

final : pdf
	@mkdir -p final
	@find lessons -type f -name 'eda-??-final.pdf' -exec cp -av {} final/ \;

# git
check:
	git add --all --verbose --dry-run

stage:
	git add --all

commit:
	git add --all
	git commit -m "course 2024 - setting up"

push:
	git push origin master

pull:
	git pull

log:
	git log --graph --oneline --decorate --all
#git log --pretty=oneline

tag:
#	git tag -a ac-2020 -m "course 2020 ended - needs revision"
# OK, never revised, now preparing next year course
	git tag -a eda-2023 -m "final version of course 2022-2023 (XXXVIII ciclo)"
	#https://blog.daftcode.pl/how-to-become-a-master-of-git-tags-b70fbd9609d9
	git push origin --tags
# simple branch workflow
# git checkout -b lesson05
#
# merging LESSONS
#pdfunite /home/eda/eda-2020/lessons/05/eda-05.pdf /home/eda/eda-2020/lessons/06/eda-06.pdf eda-2020-05-14.pdf
#pdftk /home/eda/eda-2020/lessons/05/eda-05.pdf /home/eda/eda-2020/lessons/06/eda-06.pdf cat output eda-2020-05-14.pdf
#pdftk `find lessons/ -type f -name 'eda-0[78].pdf'` cat output final/eda-2020-05-21.pdf
